var server = require("./server");
var logHandler = require("./logHandler");

function forward(handler, response, postdata, querystring) {
  if (postdata) {
    handler.post(response, postdata);
  } else {
    handler.get(response, querystring);
  }
}

function ignore(response) {
  response.writeHead(404, { "Content-Type": "text/plain" });
  response.write("404 Not found");
  response.end();
}

function log(response, postdata) {
  try {
    logHandler.log(postdata);
    response.writeHead(200, {
      "Content-Type": "text/plain",
      "Access-Control-Allow-Origin": "*"
    });
    response.write("200 OK");
  } catch (e) {
    response.writeHead(500, { "Content-Type": "text/plain" });
    response.write("Internal server error : " + e);
  }

  response.end();
}

exports.ignore = ignore;
exports.log = log;
