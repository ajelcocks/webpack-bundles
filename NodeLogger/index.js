var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");

var handle = {};
handle["/"] = requestHandlers.start;
handle["/log"] = requestHandlers.log;
handle["/favicon.ico"] = requestHandlers.ignore;

/*
 * argv contains the command line arguments
 *  1st is 'node', used to start node
 *  2nd is index.js, the name of this file
 *  subsequent are custom, this code expects the port #
 */
var port = process.env.DEV_LOG || 8787;

server.start(router.route, handle, port);
