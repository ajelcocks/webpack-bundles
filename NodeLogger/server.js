var http = require("http");
var url = require("url");

function start(route, handle, port) {
  function onRequest(request, response) {
    var postData = "";
    var url_parts = url.parse(request.url, true);
    var pathname = url_parts.pathname;
    var querystring = url_parts.query;

    request.setEncoding("utf8");

    request.addListener("data", function(postDataChunk) {
      postData += postDataChunk;
    });

    request.addListener("end", function() {
      if (request.method === "OPTIONS") {
        var headers = {};
        // IE8 does not allow domains to be specified, just the *
        // headers["Access-Control-Allow-Origin"] = req.headers.origin;
        headers["Access-Control-Allow-Origin"] = "*";
        headers["Access-Control-Allow-Methods"] = "POST, PUT, OPTIONS";
        headers["Access-Control-Allow-Credentials"] = false;
        headers["Access-Control-Max-Age"] = "86400"; // 24 hours
        headers["Access-Control-Allow-Headers"] =
          "JSNLog-RequestId, X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept";
        response.writeHead(200, headers);
        response.end();
      } else {
        route(
          handle,
          pathname,
          response,
          decodeURIComponent(postData),
          querystring
        );
      }
    });
  }

  http.createServer(onRequest).listen(port);
  console.log("Log server started on port " + port + ".");
}

exports.start = start;
