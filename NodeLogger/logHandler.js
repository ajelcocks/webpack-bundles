var winston = require("winston");
var fs = require("fs");
var mkdirp = require("mkdirp").mkdirp;

//JSNLog has TRACE 1000, DEBUG 2000, INFO 3000, WARN 4000, ERROR 5000, FATAL 6000
var customLevels = {
  levels: {
    TRACE: 0,
    DEBUG: 1,
    INFO: 2,
    WARN: 3,
    ERROR: 4,
    FATAL: 5
  },
  colors: {
    // white, grey, black, blue, cyan, green, magenta, red, yellow
    // An invalid color name here will result in 'undefined' in the log
    TRACE: "gray",
    DEBUG: "white",
    INFO: "green",
    WARN: "yellow",
    ERROR: "red",
    FATAL: "magenta"
  }
};

var logfile = getFilename();
mkdirp(logfile.dir, 0755, function(err) {
  if (err) {
    console.log(err);
  }
});

function getFilename() {
  var dt = new Date();
  var month = pad(dt.getMonth() + 1);
  var day = pad(dt.getDate());
  var hour = pad(dt.getHours());
  var min = pad(dt.getMinutes());
  var sec = pad(dt.getSeconds());
  var path = "./logs/Nuance/";
  var file = `${dt.getFullYear()}-${month}-${day}_${hour}-${min}-${sec}.log`;
  var filepath = path + file;
  return { dir: path, filename: filepath };
}

function pad(arg) {
  return arg > 9 ? arg : "0" + arg;
}

/**
 * {
 *   r:"uuid",
 *   lg:[
 *     {
 *       l:"level",
 *       m:"message",
 *       n:"logger name",
 *       t:"timestamp",
 *       u:"sequence number"
 *     }
 *   ]
 * }
 *
 * @param json The log message
 */
function log(json) {
  try {
    var jsObject = JSON.parse(json);

    if (jsObject.lg && Array.isArray(jsObject.lg)) {
      for (var message of jsObject.lg) {
        let level = getLevel(message);
        let msg = `${jsObject.r} ${getMessage(message.m)}`;
        logger.log(level, msg);
      }
    } else {
      logger.log("INFO", json);
    }
  } catch (e) {
    logger.log("ERROR", e.name + ": " + e.message);
    logger.log("INFO", json);
    throw e;
  }
}

function getMessage(message) {
  let msg = message;
  try {
    json = JSON.parse(message);
    if (json.errorMsg) {
      msg = `${json.t} ${json.u} ${json.msg}: ${json.errorMsg}`;
    }
  } catch (e) {}
  return msg;
}

function getLevel(message) {
  let level = "INFO";
  switch (message.l) {
    case 6000:
      level = "FATAL";
      break;
    case 5000:
      level = "ERROR";
      break;
    case 4000:
      level = "WARN";
      break;
    case 3000:
      level = "INFO";
      break;
    case 2000:
      level = "DEBUG";
      break;
    case 1000:
    default:
      level = "TRACE";
      break;
  }
  return level;
}
function padLeft(val, num, char) {
  var spc = "";
  for (var x = 0; x < num - val.length; x++) {
    spc += char;
  }
  return spc + val;
}

var logger = winston.createLogger({
  // error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5
  level: "FATAL",
  levels: customLevels.levels,
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        //        winston.format.timestamp(),
        winston.format.colorize(),
        winston.format.simple()
      )
    }),
    new winston.transports.File({
      filename: logfile.filename
    })
  ]
});

winston.addColors(customLevels.colors);

log(`{"r":"", "lg":[{"l":3000,"m":"${logfile.filename}"}]}`);

exports.log = log;
