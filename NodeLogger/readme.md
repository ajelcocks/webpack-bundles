# Start the logger

Start the logger using the startup.command, or a shortcut to it. This will open a terminal window.

# debugging the server

Use the `.vscode/launch.json` to start the log server in debug mode, either by clicking the debug `play` button in the (top, left) debug view or by typings `debug: open launch.json` from the command palette.

`REMEMBER` to kill the server using the stop command in the debugger toolbar or debug: disconnect from the command palette
