import i18next from "i18next";
import * as constants from "./constants";

/**
 * @class
 */
export default class App {
  /**
   * @example None
   */
  constructor() {
    this.sequence = 1;
    i18next.on(constants.event.LANGUAGE_CHANGE, () => {
      this.translate();
    });
    return this.buildUi();
  }

  /**
   * @example None
   * @returns {undefined} Nothing.
   */
  buildUi() {
    this.title = document.createElement(constants.tag.DIV);
    this.btn = document.createElement(constants.tag.BUTTON);
    let contents = document.createElement(constants.tag.DIV);
    contents.appendChild(this.title);
    contents.appendChild(this.btn);

    this.translate();

    this.btn.onclick = () => {
      this.doclick();
    };
    return contents;
  }

  /**
   * Populate the text elements using i18n constants.
   *
   * @example None.
   * @returns {undefined} Nothing.
   */
  translate() {
    let title = i18next.t(constants.i18n.TITLE);
    this.title.innerText = window.title = title;
    this.btn.innerText = i18next.t(constants.i18n.CLICK_ME);
  }

  /**
   * @example None
   * @returns {undefined} Nothing.
   */
  doclick() {
    let lang = i18next.language;
    i18next.changeLanguage(
      lang === constants.locale.FR ? constants.locale.EN : constants.locale.FR
    );
  }
}
