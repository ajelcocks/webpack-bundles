export const attr = {
  NONE: "none",
  INLINE_BLOCK: "inline-block",
  HIDDEN: "hidden"
};

export const event = {
  CLICK: "click",
  LANGUAGE_CHANGE: "languageChanged"
};

export const tag = {
  DIV: "div",
  BUTTON: "button"
};

export const i18n = {
  TITLE: "t:title",
  CLICK_ME: "t:clickMe"
};

export const locale = {
  FR: "fr",
  EN: "en"
};
