import "./app.css";
import "./app.scss";
import App from "./app.js";
import { JL as logger } from "jsnlog";
import i18next from "i18next";
import i18nextXHRBackend from "i18next-xhr-backend";
import i18nOptions from "./i18nOptions";

const uuid = "35F7416D-86F1-47FA-A9EC-547FFF510086";

logger.setOptions({
  defaultAjaxUrl: "/log",
  requestId: uuid
});

i18next
  .use(i18nextXHRBackend)
  .init(
    i18nOptions,
    /**
     * Can recieve up to two parameters, (err, t).
     *
     * @example None.
     * @returns {undefined} Nothing.
     */
    function() {
      updateContent();
    }
  )
  .on(
    "initialized",
    /** Can receieve one parameter, options.
     *
     * @example None.
     * @returns {undefined} Nothing.
     */
    function() {
      document.getElementById("contents").appendChild(new App());
    }
  );

i18next.on("languageChanged", () => {
  updateContent();
});

/**
 *
 * @example None.
 * @returns {undefined} Nothing.
 */
let updateContent = function() {
  if (i18next.language) {
    logger().info("Language changed to " + i18next.language);
  }
};
