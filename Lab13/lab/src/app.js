/**
 * @class
 */
export default class App {
  /**
   * @example None
   */
  constructor() {
    return this.buildUi();
  }

  /**
   * @example None
   * @returns {undefined} Nothing.
   */
  buildUi() {
    let contents = document.createElement("div");

    contents.innerText = "This is an example PWA";

    const btn = document.createElement("button");
    btn.innerText = "Click me";
    contents.appendChild(btn);

    btn.onclick = this.doclick;
    return contents;
  }

  /**
   * @example None
   * @returns {undefined} Nothing.
   */
  doclick() {
    // add code here
  }
}
