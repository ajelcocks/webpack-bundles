import "whatwg-fetch";

/**
 * The weather service
 */
export default class WeatherService {
  /**
   * @example None.
   * @param {string} woeid - The woeid.
   * @returns {undefined} Nothing.
   */
  static getForecast(woeid) {
    var url = this.getUrl(woeid);

    return fetch(url, { method: "GET" })
      .then(response => {
        return response.json();
      })
      .then(data => {
        return data.query;
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  /**
   * @example None.
   * @param {string} woeid - The woeid.
   * @returns {string} The url used to retrieve weather data for the given woeid.
   */
  static getUrl(woeid) {
    return (
      "https://query.yahooapis.com/v1/public/yql?format=json&q=select * from weather.forecast where woeid=" +
      woeid
    );
  }
}
