# Prerequisites

Kill any processes and close any terminals left from previous labs.

```
npm install
```

Terminal > Run task... > Lab 13 build

Note: we run build and test in an external window so we can open the integrated terminal with the `DEBUG CONSOLE` tab visible when debugging the client (browser). Any JavaScript errors in the browser will be reported here. If it is open at the TERMINAL tab we will not see any build or unit test errors.

3. Select `Lab13 Chrome` from the debug launch bar
   This will open the application in Chrome

4. Change the wording in `lab/src/app.js` to confirm everything is working.
   Change it back!

## Internationalization

## Changes since last lab

The following files and installs have already been added to the lab:

1. npm install --save i18next i18next-xhr-backend

Note: These have been installed using --save since they must be compiled into the client.

2. A new file in root folder named `i18nOptions`
   This configures the environment for the various locales.

Open `i18nOptions`.

The salient points are that the namespace (defined as `ns:["lab"]`) defines how the values will be accessed in code, e.g. `javascript i18next.t("lab:name")` and the `backend` attribute defines the name structure of the files: `/locales/{{lng}}/{{ns}}.json`

Note that returnObjects has been set to true. This allows objects to be defined on the left side of the assignment, such as arrays.

Open the `locales` folder to see the files.

The files are named as defined by the `ns` attribute. Since this is an array we can have several files named by their namespace and accessed in code with that prefix.

3. Configuration
   Open `index.js`

Three imports have been added for i18next, i18nextXHRBackend and i18nOptions

Code has been added to configure i18next and to initialize the application with the default language.

The insertion of the app has been moved into the initialized event of i18next.

```javascript
document.getElementById("contents").appendChild(new App());
```

4. `CopyWebpackPlugin` has been added to the project

```
npm install --save-dev copy-webpack-plugin
```

This copies the `locale` and `image` folders to the `dist` folder. The images are required later.

## Preparation

We use the i18next module to translate text.

add an import to `app.js`
```JavaScript
import i18next from "i18next";
```

### Encapsulate text

Each translatable piece of text must be accessible via the dom (or an object managing the dom element).

`app.js` currently adds the prompt as innerText. This could be accessible using textNode but it would be better to encapsulate the text and assign a variable.

Replace the `buildUi()` function with the following code to add the title as a div and append that before the button.

```javascript
  buildUi() {
    this.title = document.createElement("div");
    this.btn = document.createElement("button");
    let contents = document.createElement("div");
    contents.appendChild(this.title);
    contents.appendChild(this.btn);

    this.translate();

    this.btn.onclick = this.doclick;
    return contents;
  }
```

Note the elements are assigned to instance variables so they can be referenced later.

### Abstract the text assignment

1. Instead of assigning the text in `buildUi` we abstract the assignment into a function which will also be executed when the language selection changes.

```javascript
  /**
   * Populate the text elements using i18n constants.
   *
   * @example None.
   * @returns {undefined} Nothing.
   */
  translate() {
    let title = i18next.t("lab:title");
    this.title.innerText = window.title = title;
    this.btn.innerText = i18next.t("lab:clickMe");
  }
```

2. We now need to update the click action to change the language.

```javascript
  doclick() {
    let lang = i18next.language;
    i18next.changeLanguage(lang === "fr" ? "en" : "fr");
  }
```

3. Lastly, we must react to the language change. To do this we must add a (callback) function to i18next. We put this in the constructor.

```javascript
  constructor() {
    i18next.on("languageChanged", () => {
      this.translate();
    });
    return this.buildUi();
```

When the changes are saved the ui and build will automatically run.

Click the button to see the translation in action.

# Constant abstraction

There are several instances of string constants used in the code that would probably be better abstracted into a single location and accessed in the code.

Open `app.js`

1. add an import for `constants.js`

Note that since there are multiple exports and no default we can either code

```javascript
import { event, tag, i18n as i18Constants } from "./constants";
```

or

```javascript
import * as constants from "./constants";
```

2. Change all the hard-coded strings to use the constants.

Note that the `constants.js` file exists and the values will be in the intellisense popup provided by VS Code. You can also press Ctrl + space at any time to bring up the intellisense window.

Note. The UI will update when the file is saved.
