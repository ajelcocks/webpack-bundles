import { expect } from "chai";
import sinon from "sinon";

describe("App tests:", function() {
  it("canary test", () => {
    expect(true).to.be.true;
  });

  describe("The bootstrap test", function() {
    let sandbox;

    beforeEach(() => {
      sandbox = sinon.createSandbox();

      global.document = { getElementById: function() {} };
    });

    afterEach(() => sandbox.restore());

    it("Lab renders App into contents", () => {
      const getElementByIdStub = sandbox
        .stub(document, "getElementById")
        .withArgs("contents")
        .returns({});

      require("../src/app");

      expect(getElementByIdStub.called).to.be.true;
    });
  });
});
