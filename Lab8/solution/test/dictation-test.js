import { expect } from "chai";
import Dictation from "../src/dictation";

const SIGNED = "signed";
const UNSIGNED = "unsigned";

describe("dictation", function() {
  let dictation;

  beforeEach(() => (dictation = new Dictation(1)));

  it("has status", () => {
    expect(dictation.status).to.be.eql(1);
  });

  it("has " + SIGNED + " status", () => {
    expect(dictation.state).to.be.eql(SIGNED);
  });

  it("is " + UNSIGNED, () => {
    dictation.state = UNSIGNED;
    expect(dictation.state).to.be.eql(UNSIGNED);
  });

  it("is " + SIGNED, () => {
    dictation.state = SIGNED;
    expect(dictation.state).to.be.eql(SIGNED);
  });

  it("is undefined", () => {
    dictation.status = 2;
    expect(dictation.state).to.be.eql(undefined);
  });
});
