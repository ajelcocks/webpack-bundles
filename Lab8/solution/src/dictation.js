export default class Dictation {
  constructor(status) {
    this.status = status;
  }

  get state() {
    if (this.status === 0) {
      return "unsigned";
    } else if (this.status === 1) {
      return "signed";
    }
  }

  set state(state) {
    this.status = state === "signed" ? 1 : 0;
  }
}
