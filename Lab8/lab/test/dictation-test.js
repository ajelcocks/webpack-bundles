import { expect } from "chai";
import Dictation from "../src/dictation";

const SIGNED = "signed";
const UNSIGNED = "unsigned";

describe("dictation", function() {
  let dictation;

  beforeEach(() => (dictation = new Dictation(1)));

  xit("has status", () => {
    expect(dictation.status).to.be.eql(1);
  });

  xit("has " + SIGNED + " status", () => {
    expect(dictation.state).to.be.eql(SIGNED);
  });

  xit("is " + UNSIGNED, () => {
    dictation.state = UNSIGNED;
    expect(dictation.state).to.be.eql(UNSIGNED);
  });

  xit("is " + SIGNED, () => {
    dictation.state = SIGNED;
    expect(dictation.state).to.be.eql(SIGNED);
  });

  xit("is undefined", () => {
    dictation.status = 2;
    expect(dictation.state).to.be.eql(undefined);
  });
});
