# Prerequisites

Kill any processes and close any terminals left from previous labs.

```
npm install
```

# Testing with mocha and chai

## Install required modules

Mocha is a test framework supporting BDD and TDD.

Chai is an assertion library.

Sinon provides spies, stubs, and mocks.

cross-env copes with operating system differences when issuing commands to node

```
npm install --save-dev babel-core babel-register mocha chai mocha-clearscreen-reporter sinon cross-env
```

## Configure the environment

Open `.eslintrc` and add an item to the `env` section:

```javascript
"mocha": true
```

Add the following to the `scripts` section of `package.json`

```javascript
    "test": "cross-env NODE_ENV=test mocha --require babel-core/register --require babel-register --watch --reporter mocha-clearscreen-reporter"
```

```
npm run test test/canary-test.js
```

```javascript
/*
Note that terminal is now locked until Ctrl+C is pressed.
Another terminal can be opened by right-clicking the project and selecting "Open in Terminal"
*/
```

Look at the terminal output

Fix canary test (change false to true)

Test should auto run because of `--watch`

Look at the terminal output

Remove the leading `x` from `xit(` on the second test

Look at the terminal output

Add the following to `app.js`

```javascript
document.getElementById("contents").innerText = "Hello Mocha";
```

Look at terminal output - see that `2 passing`

`Ctrl+C` in the terminal to kill the test watcher

```
npm run test
```

see 5 pending

Open `dictation-test.js` and activate the first test (remove the x) and save the change

(see it fail)

Add the following to `dictation.js`:

```javascript
export default class Dictation {
  constructor(status) {
    this.status = status;
  }
}
```

(see it pass)

As the project grows we will only want to monitor the tests for the items we are working on.

Kill the terminal (ctrl+c) and type:

```
npm run test test/dict*
```

Add the following to the dictation class:

```javascript
  get state() {
    if (this.status === 0) {
      return "unsigned";
    } else if (this.status === 1) {
      return "signed";
    }
  }

  set state(state) {
    this.status = state === "signed" ? 1 : 0;
  }
```

kill the process (Ctrl+C in terminal)

# Code Coverage

istanbul is a javascript code coverage library

```
npm i --save-dev --save-exact istanbul@>1.0.0-alpha
```

Add the following to the `scripts` section of `package.json`:

```javascript
"coverage": "cross-env NODE_ENV=test istanbul cover ./node_modules/mocha/bin/_mocha -- --require babel-core/register --require babel-register"
```

add coverage to `.gitignore`

```
npm run coverage
```

Look at terminal

Expand the `coverage/lcov-report` folder and drag `index.html` to the browser
or right-click and select copy path then paste that into the browser address bar.

Note that `dictation.js` is not 100%

Remove the x from xit in the tests. Run coverage to see the effect of each test.
