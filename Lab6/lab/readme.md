# Prerequisites

Kill any processes and close any terminals left from previous labs.

```
npm install
```

# Code splitting

```javascript
/*
As a project evolves there will be many files which may all need the same 3rd party library. As we bundle these files it is possible the same 3rd party dependency will be included more than once, unnecessarily inflating the size of the bundle.
*/
```

To illustrate this, add 'backbone' and 'jquery' to the project:

```
npm install --save-dev backbone jquery
```

Create a file named `main.js` in the `src` folder with the following content:

```javascript
import bone from "backbone";
```

Change `app.js` to contain the same line

Change the 'entry' item in `webpack.config.js` item as follows:

```javascript
  entry: {
    main: "./src/main.js",
    app: "./src/app.js"
  },
```

Change output.filename to

```
[name].bundle.js
```

```javascript
/*
The parser will iterate each item in entry and use the output file value, substituting the [name] with that of the file.
*/
```

Build the project

```
npm run build
```

```javascript
// Maximize the terminal and note the asset sizes
```

Add the following section to `webpack.config.js`, after the output item:

```javascript
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          name: "common",
          chunks: "initial",
          minChunks: 2
        }
      }
    }
  },
```

```
npm run build
```

```javascript
// Maximize the terminal and note the asset sizes
```
