# Prerequisites

Kill any processes and close any terminals left from previous labs.

```
npm install
```

# Stylesheets

Create a file in the src folder named `app.css` with the following content:

```javascript
body {
  background-color: aqua;
}
```

Add a require statement in `app.js`

```javascript
const css = require("./app.css");
```

```
npm run build
```

Notice the error in the terminal:

<span style="color:salmon">
Module parse failed: Unexpected token (1:5)
You may need an appropriate loader to handle this file type.
</span>

```javascript
// This is because webpack only provides a default loader which does not understand CSS. We must add a loader in the configuration.
```

For CSS we need to install two loaders:

```
npm install css-loader style-loader --save-dev
```

A rule must also be added to `webpack.config.js`

```javascript
module: {
  rules: [
    {
      test: /\.css$/,
      use: ["style-loader", "css-loader"]
    }
  ];
}
```

```javascript
/*
Note that the order is important. The output of css-loader is piped into style-loader.

Also note that without style-loader the output would be in-lined in the js bundle and would not work.
*/
```

```
npm run build
```

Drag the html file from the dist folder in VS Code into a browser to see the result.

Open the dev tools element inspector and expand the <head> to see the css

A different loader is required for SASS

Create a file in the `src` folder named `app.scss` with the following content:

```css
$ink: blue;
body {
  color: $ink;
}
```

```
npm install --save-dev sass-loader node-sass
```

We must also add a loader in the module items rules collection

```javascript
{
  test: /\.scss$/,
  use: [
    "style-loader",
    "css-loader",
    "sass-loader"
  ]
}
```

add a require to app.js

```javascript
require("./app.scss");
```

By default CSS files will be bundled into the main bundle file. To configure webpack to create a separate bundle we must add the `mini-css-extract-plugin`.

```
npm install --save-dev mini-css-extract-plugin
```

An entry to require the module must be added to `webpack.config.js`

```javascript
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
```

And it must be added to the plugins section

```javascript
plugins: [
  new MiniCssExtractPlugin({
    filename: "[name].css",
    chunkFilename: "[id].css"
  })
```

Finally it must be added to the existing loader pipelines in the rules section, replacing the style-loader

```javascript
module: {
  rules: [
    {
      test: /\.css$/,
      use: [MiniCssExtractPlugin.loader, "css-loader"]
    },
    {
      test: /\.scss$/,
      use: [
        MiniCssExtractPlugin.loader,
        "css-loader",
        "sass-loader"
      ]
    },
```

```
npm run build
```

This result in a single css file named `main.css` in the dist folder.

This is included as a `link` tag in the `head` section, as can be seen in the developer tools element inspector.
