# Prerequisites

Kill any processes and close any terminals left from previous labs.

```
npm install
```

# Using images

Add a background image to the page using app.css

```css
body {
  background: url("logo.png") no-repeat center;
```

If we were to run `npm run build` now we would see an error

```
    ERROR in ./src/logo.png 1:0
    Module parse failed: Unexpected character '�' (1:0)
```

Install file-loader

```
npm i -save-dev file-loader
```

open `webpack.config.js`

remove the following line from devServer item, because it makes dev server prefix paths with `dist/`:

```javascript
contentBase: path.resolve(__dirname + "dist"),
```

add a rule to the `module.rules` section

```javascript
{
  test: /\.png$/,
  use: "file-loader"
}
```

Build the project

```
npm run build
```

The result of the build is that a file with random number is created in the `dist` folder and used in the `main.css` file.

This can also be used in the html. Open `index.ejs` and change it to:

```html
  <img src=<%= require("./logo.png") %> />Simple webpack project
```

```javascript
// Open the developer tools and look at the src and css for the image
```

If we want to use the name of the file we must change the rule as follows:

```javascript
  test: /\.(png|jpg|gif)$/,
  use: [
    {
      loader: "file-loader",
      options: {
        name: "[name].[ext]",
        outputPath: "images/"
      }
    }
  ]
```

```javascript
/*
Note that now we have the named image inside an images folder and the main.css file is updated to reflect the new name.

However, the randomly named file still exists. This is because the build does not remove what was previously generated.
*/
```

## clean up on aisle 6!

To remove the dist folder we can use the clean-webpack-plugin

```
npm install --save-dev clean-webpack-plugin
```

First we define a few constants...

```javascript
const CleanWebpackPlugin = require("clean-webpack-plugin");
const pathsToClean = ["dist"];
const cleanOptions = {
  root: __dirname,
  verbose: false
};
```

...and then use those constants in the plugin definition

```javascript
plugins: [
    new CleanWebpackPlugin(pathsToClean, cleanOptions)
```

```
npm run build
```
