const express = require("express");
const proxy = require("http-proxy-middleware");
const basicAuth = require("express-basic-auth");
const app = express();
const http = require("http");

const ports = {
  HTTP: process.env.DEV_PROXY_HTTP || 3010,
  SERVICES: process.env.DEV_DATA_SERVER || 3013,
  WEB_APP: process.env.DEV_WEB_CLIENT || 3014
};
const HOST_URL = "http://localhost:";
const IP_ADDRESS = "0.0.0.0";

const apiOptions = {
  target: HOST_URL + ports.SERVICES,
  changeOrigin: true,
  pathRewrite: {
    "^/api/": "/"
  }
};

const webOptions = {
  target: HOST_URL + ports.WEB_APP,
  changeOrigin: true
};

let auth;
/*
auth = {
  users: {
    "admin": "supersecret"
    "user": "secret"
  }
}
*/

if (auth) {
  app.use(basicAuth(auth));
}

app.use("/api", proxy(apiOptions));
app.use("/", proxy(webOptions));

var httpServer = http.createServer(app);
httpServer.listen(ports.HTTP, IP_ADDRESS);

// prettier-ignore
console.log(`Proxy listening on http port ${ports.HTTP}.`);
