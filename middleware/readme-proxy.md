#Initialize project

```
npm install --save-dev http-proxy-middleware
```

# Ports

port 3011 is the https proxy, set in the middleware project.

Note when using no-ip and a home network port 3011 is in port used for forwarding.

port 3012 is the app server, set in bs-config.js

port 3013 is the services, set in www in the server/bin folder

port 3014 is the log server, set by startup.command in the NodeLogger project.

# basic auth

basic auth can be triggered by passing a parameter in the npm invocation as:

```
user=[absolute address to file]
```

An example use in package.json would be:

```json
  "config": {
    "users": "/Users/MyProject/myBasicAuth.json"
  },
  "scripts": {
    "proxy": "cd nodemon ./src/index.js users=$npm_package_config_users",
```
