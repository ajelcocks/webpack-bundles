# Creating a Node managed project.

## Initialize Node Package Manager

First we need to setup the project dependencies to be managed by the Node Package Manager (npm).

Right-click the project folder and select `Open in Terminal`

```javascript
// Make sure that the terminal prompt corresponds to the path for the folder
```

In the terminal, type

```
npm init -y
```

## Install eslint

In the terminal, type:

```
npm install eslint --save-dev
```

## Initialize the project for eslint

```
node_modules\.bin\eslint --init
```

Answer the prompts as indicated below:

```
? How would you like to configure ESLint? Answer questions about your style
? Which version of ECMAScript do you use? ES2016
? Are you using ES6 modules? Yes
? Where will your code run? Browser
? Do you use CommonJS? No
? Do you use JSX? No
? What style of indentation do you use? Spaces
? What quotes do you use for strings? Double
? What line endings do you use? Unix
? Do you require semicolons? Yes
? What format do you want your config file to be in? JavaScript
```

Note that the invocation is from the `.bin` folder of `node_modules` and is relative to the current folder in the terminal.

However, if the eslint module had been installed globally it could be invoked just by typing `npm eslint --init` (Don't do this)

```Javascript
// Using global installs is discouraged because it is better to manage versioning per project. If the global install is updated it could break other projects.
```

## Initialize project for Prettier

VS Code has been setup to use the eslint and prettier extensions but those packages must be present in the project folder.

The eslint-config-prettier module prevents conflicts between eslint and prettier.

```
npm install --save-dev prettier eslint-config-prettier
```

Open the file named `.eslintrc.js`

Add an entry for node so that the `require()` function of node will be recognised.

```javascript
env: {
  node: true
},
```

Change the indent attribute in the rules section from 4 to 2, because Prettier uses 2.

Prettier must also be added to the `extends` item. Note that the order is important as the ouput from prettier is input to eslint, since the items are processed right to left.

```javascript
rules: {
  indent: ["error", 2],
  [...]
  extends: ["eslint:recommended", "prettier"]
}
```
