# Notes

The script names in `package.json` have changed. Most notably:

'npm run all' is now `npm run dev`






# WebpackMerge

This enables the ability to have one config derive (extend) from another.

There are now three files, base.webpack.config.js, dev.webpack.config.js and prod.webpack.config.js

It was also necessary to install the `webpack-merge` module.
```
npm install --save-Dev webpack-merge
```

This is the construct to derive from a "parent" config...
```javascript
module.exports = merge(baseConfig, {
  //overrides go here
};





# DefinePlugin

Can define globals in `webpack.config.js`...
```javascript
    new webpack.DefinePlugin({
      YOUR_GLOBALS_HERE: JSON.stringify("anything with quotes must be stringified"),
      NORMAL_STRING: "noNeedToStringify"
    })
```
...but `eslint` will complain that the global is not defined.

The 'undefined rule' can be disabled, but that's not desirable.

A 'custom' comment can be placed in the file that accesses the global, but that means a comment must be placed in every file that uses the global.
```javascript
/* global name1, name2, name3 */
```

This can be overidden in `.eslintrc.js` but that means the global must be defined twice, once in `webpack.config.js` and once in `.eslintrc.js`. Not desirable but the better than the alternatives.
```javascript
  globals: {
    GLOBAL_NAME: true
  }
```
The value can be `true` or `false`. `false` means the global is not recognized and so `eslint` will complain, essentially the same as not defining it in `.eslintrc.js` at all.





# NormalModuleReplacementPlugin

```javascript
plugins: [
    new webpack.NormalModuleReplacementPlugin(
      /\.\/services\/prod-services/,
      "./services/dev-services"
    )
```

Matches the pattern and replaces with the given string.

When this is combined with using `WebpackMerge` a different replacement can be specified for each config.

Note that when using this with imports it ts recommended that the import as coded actually exist so that the IDE will fill in the intellisense prompts.

`Important`: When a change is made to the pattern or string while running the application in debug with --watch then the app must be killed and rebuilt, since the pattern match and code substitution is made at build time.






# Decorators

VS Code flags a TypeScript error on decorators. This can be disabled by adding to the workspace settings of VS Code.

Click the gear icon in bottom left of VS Code and select `Settings` or open the command palette (mac: Ctrl+shift+p) and type `open workspace settings`
```json
{
  "folders": [
    {
      "path": "webpack-bundles"
    }
  ],
  "settings": {
    "typescript.validate.enable": false,
    "javascript.implicitProjectConfig.experimentalDecorators": true
  }
}
```

```
npm install --save-dev babel-plugin-transform-decorators-legacy
```

Add `transform-decorators-legacy` to the `plugins` collection in `.babelrc`
```json
  "plugins": ["transform-decorators-legacy", "syntax-dynamic-import"]
```
Note: The order matters!

