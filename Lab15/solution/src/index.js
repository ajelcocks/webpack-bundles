import "./app.css";
import "./app.scss";
import { JL as logger } from "jsnlog";
import App from "./app";
import i18next from "i18next";
import i18nextXHRBackend from "i18next-xhr-backend";
import { i18nOptions } from "./i18nOptions";
import * as constants from "./constants";

const uuid = require("uuid/v1")();

logger.setOptions({
  defaultAjaxUrl: "log",
  requestId: uuid
});

window.addEventListener("error", function(msg) {
  logger().fatal(msg);
  return false;
});

i18next
  .use(i18nextXHRBackend)
  .init(
    i18nOptions,
    /**
     * Can recieve up to two parameters, (err, t).
     *
     * @example None.
     * @returns {undefined} Nothing.
     */
    function() {
      updateContent();
    }
  )
  .on(
    "initialized",
    /** Can receieve one parameter, options.
     *
     * @example None.
     * @returns {undefined} Nothing.
     */
    function() {
      new App();
    }
  );

i18next.on(constants.event.LANGUAGE_CHANGED, () => {
  updateContent();
});

/**
 *
 * @example None.
 * @returns {undefined} Nothing.
 */
let updateContent = function() {
  if (i18next.language) {
    logger().info("Language changed to " + i18next.language);
  }
};
