import { JL as logger } from "jsnlog";
/**
 *
 * @param {string} name - The name of the item being logged.
 * @returns {string} The result of executing the funcion being decorated.
 */
export default function log(name) {
  return function decorator(t, n, descriptor) {
    const original = descriptor.value;
    if (typeof original === "function") {
      descriptor.value = function(...args) {
        logger().log(3000, `${name} invoked with ${args}`);
        try {
          const result = original.apply(this, args);
          logger().log(3000, `Result from ${name} - ${result}`);
          return result;
        } catch (e) {
          logger().log(3000, `Error in ${name} - ${e}`);
          throw e;
        }
      };
    }
    return descriptor;
  };
}
