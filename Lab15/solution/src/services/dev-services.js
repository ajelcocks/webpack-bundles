export const services = Object.freeze({
  SERVICE_PREFIX: "/api/",
  discovery: {
    PREFIX: "",
    URL: "api/DiscoverLogin",
    LOGIN: "api/institutioninfo/"
  }
});
