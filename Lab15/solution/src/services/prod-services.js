export const services = Object.freeze({
  SERVICE_PREFIX: "/api/1.0/",
  discovery: {
    PREFIX: "https://enterpriseservices.nuancehce.com/api/1.0/",
    URL: "discovery/1.0/services/DiscoverLogin?majorVersion=1.0.0",
    LOGIN: "institutioninfo/1.0/institutions/"
  }
});
