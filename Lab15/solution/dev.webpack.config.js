const webpack = require("webpack");
const merge = require("webpack-merge");
const baseConfig = require("./base.webpack.config.js");

module.exports = merge(baseConfig, {
  mode: "development",
  devtool: "eval-source-map",
  plugins: [
    new webpack.NormalModuleReplacementPlugin(
      /\.\/services\/prod-services/,
      "./services/dev-services"
    )
  ]
});
