const merge = require("webpack-merge");
const baseConfig = require("./base.webpack.config.js");

module.exports = merge(baseConfig, {
  mode: "production"
});
