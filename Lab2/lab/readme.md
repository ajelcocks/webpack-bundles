# Prerequisites

```
npm install
```

# Building a project using webpack

The `webpack` and `webpack-cli` modules are required to build a project.

Open a terminal for the project (Lab2/lab)

1.  Install the modules

```
npm install webpack webpack-cli --save-dev
```

2.  Create a folder named `src` in the project folder

3.  Create a file named `app.js` in the `src` folder with the following content:

```javascript
console.log("Hello Webpack");
```

4.  Initialize the webpack configuration

Create a file in the project directory named `webpack.config.js` with the following content:

```javascript
const path = require("path");

module.exports = {
  entry: "./src/app.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "app.bundle.js"
  }
};
```

5.  Build the project
    open package.json and add the following:

```javascript
"scripts": {
  "build": "webpack -d"
}
```

Type the following command into the terminal:

```
npm run build
```

```javascript
//note the output is in the dist folder.
```
