var express = require("express");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cors = require("cors");

var cityRouter = require("./routes/city");

var app = express();
app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use("/city", cityRouter);

/* eslint-disable no-unused-vars */

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.status(404).send("Not found");
});

// error handler
app.use(function(err, req, res, next) {
  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
