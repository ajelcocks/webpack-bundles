module.exports = {
  server: "./dist",
  port: 3001,
  open: false,
  watchOptions: {
    ignored: "test"
  }
};
