# Prerequisites

Kill any processes and close any terminals left from previous labs.

# Web services

This lab comes pre-configured with an `Express` web server, in the `server` folder of the project.

1: Using `Express` to provide services.

2: `bin/www` configures Express

3: `server.js` configures the server and the url mappings (api)

4: `routes` contains the services (routes) which are mapped in `server.js`

5: Uses `morgan` for logging

6: Turns eslint console error off using `.eslintrc.js` in the `server` folder

# Using the service

Add a folder named `services` to the `src` folder

Create a file named `city-service.js` in the services folder with the following content:

```javascript
import "whatwg-fetch";

export default class CityService {
  static get() {
    return fetch("http://localhost:3000/city", {
      method: "GET"
    })
      .then(response => response.json())
      .catch(function(error) {
        console.log(error);
      });
  }
}

/*
Note that the Express server providing the services is started on port 3000, as defined in server/bin/www.
*/
```

The service is using `fetch` which is not supported by all browsers.

```
npm install --save whatwg-fetch
```

```javascript
/*
The fetch() function is a Promise-based mechanism for programmatically making web requests in the browser. This project is a polyfill that implements a subset of the standard Fetch specification, enough to make fetch a viable replacement for most uses of XMLHttpRequest in traditional web applications.
*/
```
Note we use `--save` for this since it is generating code we need to deploy in the client. Open `package.json` and scroll to the bottom to see.

# Abstracting the application

A file named `app.js` has been added to the `src` folder. It encapsulates the ui component currently created in `index.js`.

Doing this positions the application to insert different components based on some logic such as user privileges.

Change `index.js` to use that file. `Replace` the contents with the following:

```javascript
import "./app.css";
import "./app.scss";
import App from "./app.js";

document.getElementById("contents").appendChild(new App());
```

# One terminal to rule them all

As we have seen previously we can open multiple terminals and start the build, test and (now) the services.

This is awkward to manage. `npm-run-all` provides the ability to execute multiple scripts as one command and can be started synchronously or in parallel. The result being that all output is in one terminal.

Synchronous is not good for our purposes because each of the scripts do not terminate, and so the second and subsequent scripts would not run.

```
npm install --save-dev npm-run-all
```

An entry has already been added into the scripts section of `package.json` for this.

`"all"`: `"npm-run-all -pr build start test"`,

```javascript
/*
-p, --parallel <tasks>
  Run a group of tasks in parallel, where <tasks> is the space delimited list of script names to run.
  e.g. 'npm-run-all -p foo bar' is similar to 'npm run foo & npm run bar'.

-r, --race
  Set the flag to kill all tasks when a task finished with zero.
  This option is valid only with 'parallel' option.
*/
```

## Debugging

## launch configurations

Open the `launch.json` file in the `.vscode` directory.

The launch file contains one or more debug configurations that can be `launched`.

Note that the port specified in the `Lab10 Chrome` launch script must match that of the `lite-server` which is serving the web application.

## lite-server port

By default, `lite-server` auto assigns port `3001` but can be set to a specific number by adding `port:nnnn` to `bs-config.js`:

Open `bs-config.js` and you will see...

```javascript
module.exports = {
  server: "./dist",
  port: 3001,
```

## Debug Panel

Switch the VS Code view to debug (4th icon on left)

Note the drop-down menu at the top of the debug panel (on the top-left)

The drop-down contains entries which correspond to the items defined in `launch.json`

## Attaching a debugger to a node.js process.

VS Code will automatically attach a debugger to any `node.js ` process it sees. We don't want that. 

If `Auto Attach: On` is present in the blue status bar at the bottom of VS Code click it to set `auto attach` to `off`. If it is not present - even better!

Open the settings and type `attach` in the search bar.

Select `disabled` for `Node: Auto Attach`.

This can be re-enabled at any time, should it be required, by changing the setting back without need to restart VS Code. 

## breakpoints

A break point is set in code by clicking in the `gutter` to the left of the line number in file view.

Open the file panel and add a break point in `server/routes/city.js` on the `res.json(cities)` statement (line 13)

Set a break in `src/app.js` on the `alert(data.city)` statement (line 26)

## Launch sequence

1. Start the build, test and app server by typing `npm run all` in the VS Code terminal.

   This will start the `build`, `test runner` and `web application server` with the output all in one terminal.
   
   Review the output in the TERMINAL panel at the bottom of VS Code.

2. Select the debug panel and start the data server by selecting `Lab10 server` from the launch selector, then press the green (start) triangle.

   1. Note that the VS Code debugger will attach to the node server. The blue bar turns orange and the debug bar is visible.
    
   2. The default debug bar provided by VS Code is obtrusive and cannot be configured to resolve that, so we use the Statusbar debugger extension.

      The extension places a debug bar in the in the bottom (orange gutter) of VS Code.

   3. Debug commands are also available via keyboard shortcuts and from the VS Code menu.

   4. Look at the DEBUG CONSOLE tab in the terminal panel.

   5. Look at the BREAKPOINTS section of the debug panel in the bottom left of the editor.
   
2.  Open the browser using the `Lab10 Chrome` launch selector at the top of the debug view.

3.  Switch to the browser and click the button. The breakpoint in the `data service` will be hit.

    ```javascript
    // Note that the debug bar shows the server config
    ```

4.  Click continue in the debug toolbar or press F5.

    The break in the `client code` will be hit.

    ```javascript
    // Note the debug bar has switched to the client config.
    ```

5.  Note The CALL STACK item in the DEBUG panel

    This shows two items are currently monitored. When a break point is hit the call stack is rendered under the relevant item.

    An entry in the call stack can be clicked to go to the relevant line of code.

6.  Click the stop button (twice) in the debug bar or press Shift+F5 (twice)

7. Right-click in the BREAKPOINTS section of the debug panel and select `remove all breakpoints`
