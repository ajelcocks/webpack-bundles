export default class LogMessage {
  constructor(level, applicationName, sequenceNumber, uuid, message) {
    this.level = level;
    this.applicationName = applicationName;
    this.sequenceNumber = sequenceNumber;
    this.uuid = uuid;
    this.message = message;
  }

  get json() {
    return {
      level: this.level,
      aplicationName: this.applicationName,
      sequenceNumber: this.sequenceNumber,
      uuid: this.uuid,
      message: this.message
    };
  }
}
