var express = require("express");
var router = express.Router();

let cities = {
  iwoeid: 2367105,
  city: "Boston",
  state: "Massachusetts",
  country: "United States"
};

/* GET users listing. */
router.get("/", function(req, res) {
  res.json(cities);
});

module.exports = router;
