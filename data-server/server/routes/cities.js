let express = require("express");
let router = express.Router();

let json = [{ name: "Boston" }];

router.get("/", function(req, res) {
  res.json(json);
});

module.exports = router;
