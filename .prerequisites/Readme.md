# Prerequisites

## Install Node.js

Install the latest version of node from https://nodejs.org/en/

## Install git

Install the latest version of git from https://git-scm.com/downloads

## Install VS Code

If you have not already done so, install VS Code from https://code.visualstudio.com/download

# Setting up VS Code

## Open VS Code

## Configure settings

Click the gear icon in the bottom left of the IDE and select _Settings_ or select _Code > Preferences > Settings_ from the VS Code menu

```javascript
{
  "debug.toolBarLocation": "hidden",
  "editor.formatOnPaste": false,
  "editor.formatOnSave": true,
  "editor.minimap.enabled": false,
  "editor.tabSize": 2,
  "explorer.confirmDelete": false,
  "files.associations": {
    "*.ejs": "html"
  },
  "files.eol": "\n",
  "git.autofetch": true,
  "git.path": "/usr/local/git/bin",
  "gitlens.advanced.messages": {
    "suppressShowKeyBindingsNotice": true
  },
  "javascript.updateImportsOnFileMove.enabled": "always",
  "statusbarDebugger.template": "",
  "telemetry.enableTelemetry": false,
  "telemetry.enableCrashReporter": false,
  "terminal.integrated.env.osx": {},
  "terminal.integrated.env.windows": {},
  "terminal.integrated.shell.windows": "C:\\Windows\\System32\\cmd.exe",
  "workbench.startupEditor": "none"
}
```

## VS Code plugins

Click the last (5th) icon (a square) in the toolbar at left.

### ESLint

Type eslint in the search box provided. Select ESLint by Dirk Baeumer

### Prettier

Type Prettier in the search boc provided. Select Prettier - Code formatter by Esben Petersen

### Debugger for Chrome

Type chrome in the search box provided. Select Debugger for Chrome by Microsoft

### EJS language support

Type EJS language in the search box provided. Select EJS language support by DigitalBrainstem

### StatusBar Debugger

Type StatusBar debugger in the search box provided. Select StatusBar Debugger by Fabio Spampinato

### TODO Highlighter

Type TODO highlight in the search box provided. Select TODO Highlight by Wayou Liu

### Gitlens [optional]

Type gitlens in the search box provided. Select GitLens by Eric Amodio

### Stausbar debugger

Type statusBar Debugger in the search box. Select the one by Fabio Spampinato

# Download labs

Start VS Code

Clear IDE - `File > Close workspace` (OSX: Cmd+K F) if you have a workspace open

Open command palette - `View > Command palette` (OSX: Cmd+Shift+P)

Select `clone remote` (type `git:clone`)

Enter the remote url https://ajelcocks@bitbucket.org/ajelcocks/webpack-bundles.git

Enter credentials, if required

Select the folder that will contain the project folder.
It is recommended to use all lowercase with no spaces and to use dash as a separator, e.g. webpack-bundles

Reply `yes` to open the repository

Select 'Save workspace' from the file menu

Save the workspace in the same folder that contains the project, with the same name as the project.
