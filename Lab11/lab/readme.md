# Prerequisites

Kill any processes and close any terminals left from previous labs.

Include the `middleware` and `data-server` projects folders in the workspace, `File > Add folder to workspace`

# Prologue

In the production environment the various components, such as the data server and logger, can be deployed 'anywhere'

The client should not be aware of deployment details, such as port, and should only access using relative addressing unless it is a fixed external service (Google geo location, Yahoo weather service, etc.)

## data services

The data server has been removed from the previous lab and is now a separate project.

## proxy

A new middleware project has been created. This is a proxy which will forward requests to the relevant services.

Currently the ports are:

- 3010 - The http proxy server (see `middleware/src/index.js`)
- 3013 - The data server (see `data-server/server/bin/www`)
- 3014 - The application server (see `lab/bs-config.js`)

The port in `bs-config.js` has been changed from the previous lab to `3014`

## lite-server UI

The lite-server/BrowserSync provides a management UI, this will be seen reported in the terminal when the application is started.

```
-------------------------------------
       Local: http://localhost:3001
    External: http://192.168.1.13:3001
 -------------------------------------
          UI: http://localhost:3002
 UI External: http://192.168.1.13:3002
 -------------------------------------
```

This has been disabled by adding `ui: false` to `bs-config.js`

## Multi-folder projects in VS Code

When developing you may want to debug both the web application and the data server.
Simply add the project folder to VS Code. If you want you can save it as a seperate code-workspace, so you could have one workspace for each of the projects and one for the development suite.

Pros:

1. Allows debugging in one interface
2. Code management in one place
3. git management in one interface

Cons:

1. All the launch configs appear in the drop list.
2. The terminals can get messy. Debug can launch in external but task cannot, so we must launch a .bat / command file.
3. Save the workspace with an appropriate name, CodeLabs.code-workspace for example.
   This forms the basic template for any Plain Old JavaScript project.

# The development environment

## Start proxy

open a terminal in the `middleware` folder.

```
npm install
```

`Terminal > Run task... > Start proxy (middleware)`

## Start data-server

open a terminal in the `data-server` folder.

```
npm install
```

`Terminal > Run task... Start data server (data-server)`

Test by entering one of the following into the browser or press Cmd and click the link:

1. direct connection - http://localhost:3013/Cities
2. proxy - http://localhost:3010/api/Cities

An alternative is to launch the data server in debug mode using the `Debug data-server` item in the select box of the debug panel.

- Kill the data-server (ctrl+c)
- Select `Debug data server (data-server)` from the DEBUG panel

Repeat setps 1 and 2 above.

```javascript
/*
Note how the status bar in VS Code is now orange.

The data service is started using nodemon, which monitors the code for changes and will restart the server.
*/
```

# Lab

Open a terminal to the lab folder.

```
npm install
```

`Terminal` > `Run task...` > `Lab 11 build (lab)`

Select `Lab 11 Chrome` from the debug launch bar

Set a break at line 7 in the service (`data-server/server/routes/cities.js`)

```javascript
Line 7: res.json(cities);
```

...and in the lab (`lab/src/app.js`) at line 27

```javascript
Line 27: alert(data);
```

Click the button in the web app (browser)

```javascript
// Note that the break in the client is hit but not the server.
```

Switch the terminal view in VS Code to the DEBUG CONSOLE

```javascript
/*
Note the error `404 (Not Found) [http://localhost:3010/api/city]`
...because the service expects `cities` not `city`.

This is also reported in the console of the inspector tool of the browser.
*/
```

Change the url in the `lab/src/services/city-service` (line 5) to

```javascript
    return fetch("api/cities", {
```

Note the build window refreshes having rebuilt the project and re-run the unit tests

The browser also refreshed.

Now click the button.

# Kill all the processes

Cloes the browser.

Note that the debug process in VS Code finishes and the gutter turns blue.

Do not close the terminals without first killing the process as this can sometimes leave the process running.

Press Ctrl+c at least once, some processes require twice and will print a notification. The best policy is to hit Ctrl + (c multiple times !)

Then close the terminal.