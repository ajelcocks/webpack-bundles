import cityService from "./services/city-service";

export default class App {
  constructor() {
    this.sequence = 1;
    return this.buildUi();
  }

  buildUi() {
    let contents = document.createElement("div");

    contents.innerText = "This is an example PWA";

    const btn = document.createElement("button");
    btn.innerText = "Click me";
    contents.appendChild(btn);

    btn.onclick = this.doclick;
    return contents;
  }

  doclick() {
    cityService
      .get()
      .then(data => {
        alert(data[0].name);
      })
      .catch(function(error) {
        alert(error);
      });
  }
}
