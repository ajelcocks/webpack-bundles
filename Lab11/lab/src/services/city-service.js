import "whatwg-fetch";

export default class CityService {
  static get() {
    return fetch("api/city", {
      method: "GET"
    })
      .then(response => response.json())
      .catch(function(error) {
        console.log(error);
      });
  }
}
