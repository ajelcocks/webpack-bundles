# Using the labs

Start VS Code

Choose the lab you wish to work on and...

1. Close the existing workspace
   File > Close workspace ( MAC: cmd+k f | Win: )

2. Add the lab folder

   - File > Add folder to workspace...
   - Select the lab folder from Lab`{n}`

3. Open the readme and follow the directions

# Labs

1. Creating a Node managed project.
2. Building a project using webpack.
3. Creating a simple web application.
4. Stylesheets
5. Images
6. Code splitting
7. Lazy loading and Babel
8. Testing with mocha and chai
9. Creating a complete JavaScript project
10. Debugging in VS Code
11. A complete development environment
12. JSDoc
13. Internationalization
14. Progressive Web Applications
15. Decorators
