const css = require("./app.css");
const sass = require("./app.scss");

function addButton() {
  let btn = document.createElement("button");
  btn.innerText = "Click me";

  btn.onclick = doClick();

  return btn;
}

document.body.appendChild(addButton());

function doClick() {
  return e =>
    import(/* webpackChunkName: "main" */ "./main")
      .then(module => {
        const print = module.default;
        print();
      })
      .catch(err => {
        return "Error loading main";
      });
}
