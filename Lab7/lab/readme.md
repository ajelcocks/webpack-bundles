# Prerequisites

Kill any processes and close any terminals left from previous labs.

```
npm install
```

# Lazy loading

Open `webpack.config.js`

Remove `main: "./src/main.js",` from `module.exports.entry`

Replace the content of `main.js` with the following:

```javascript
console.log("The main.js file has loaded");

export default () => {
  console.log("Button clicked");
};
```

Open `app.js` and replace the content with following code:

```javascript
const css = require("./app.css");
const sass = require("./app.scss");

function addButton() {
  let btn = document.createElement("button");
  btn.innerText = "Click me";

  btn.onclick = () =>
    import(/* webpackChunkName: "main" */ "./main").then(module => {
      const print = module.default;
      print();
    });

  return btn;
}

document.body.appendChild(addButton());
```

```javascript
// The comment is important as it contains commands to webpack
```

```
npm run build
```

```javascript
// Note that webpack has created main.bundle.js in the dist folder
```

Open `index.html` in the browser

Open the developer tools and make the `Elements` tab active

Expand the `head` element

```javascript
//Note that main.js is not present
```

Make the `Network` tab active

Click the button in our page

```javascript
// Note the main.js file is downloaded
```

Make the `Elements` tab active

```javascript
// Note the script tag is present in the <head> tag
```

Make the `Console` tab active

```javascript
// Note the messages have been written
```

Switch back to Network

Click the button (again)

```javascript
// Note the file was not downloaded again
```

Switch to the Console

```javascript
// Note only the button click was reported and not the "...has downloaded" message
```

Cut the fat-arrow code from onclick assignment and put it in a function, as below:

```javascript
btn.onclick = doClick();

function doClick() {
  return () =>
    import(/* webpackChunkName: "main" */ "./main").then(module => {
      const print = module.default;
      print();
    });
}
```

In this way we can abstract the dynamic load into a reusable function.

However, this causes an eslint error.

Add the following to the `parserOptions` section of `.eslintrc`:

```json
allowImportExportEverywhere: true
```

## Babel

Babel is a compiler (transpiler) compiles Javascript next into common JavaScript (using commonJs)

```
npm install --save-dev babel-core babel-loader@7 babel-preset-env babel-preset-stage-3
```

````javascript
/*
 Note the use of @7 for babel-loader.

 At the time of writing this lab there is an inconsistency in versions. Babel loader is at 8 but babel-core has not yet caught up

 (The compatible versionis currently in alpha)
 */

`babel-preset-env` is a special preset that will contain all yearly presets so user's won't need to specify each one individually. It currently includes: `es2017`, `es2016` and `es2015`.

`stage-3` is the latest completed proposal (think es2018)

add a rule to the `rules:` collection in `webpack.config.js`

```javascript
    {
      test:/\.js$/,
      use:"babel-loader",
      exclude: /(node_modules)/
    }

// We do not want to include the js files from the node modules so an exclude attribute is added
````

Create a new section in `package.json` after the dependencies.

```javascript
  "babel": {
    "presets": [
      [
        "env",
        {
          "targets": {
            "browsers": [
              "last 2 versions",
              "ie >= 7"
            ]
          }
        }
      ]
    ],
    "plugins": [
      "syntax-dynamic-import"
    ]
  }
```

Dynamic imports are still 'experimental', so we need an extra plugin.

```
npm i --save-dev babel-plugin-syntax-dynamic-import
```

Confirm changes compile

```
npm run build
```

Open the `app.js` files.

The IDE is still flagging the import as an error because we need to configure eslint to use the babel parser.

```
npm install --save-dev babel-eslint
```

Open the file named `.eslintrc` and add the `parser` item as outlined below:

```javascript
  parser: "babel-eslint",
  parserOptions: {
    ...
```

Open the `app.js` file. The import should no longer be flagged.

The babel config can be abstracted into a file named `.babelrc`

Remove the lines from `package.json` after the `dependencies` to the end

```javascript
,
  "babel": {
    "presets": [
      [
        "env",
        {
          "targets": {
            "browsers": [
              "last 2 versions",
              "ie >= 7"
            ]
          }
        }
      ]
    ],
    "plugins": [
      "syntax-dynamic-import"
    ]
  }
```

Create a file named .babelrc in the project folder with the following content:

```javascript
{
  "presets": [
    [
      "env",
      {
        "targets": {
          "browsers": ["last 2 versions", "ie >= 7"]
        }
      }
    ]
  ],
  "plugins": ["syntax-dynamic-import"]
}
```

confirm app.js is error free

confirm the build

```
nmp run build
```

refresh the browser and confirm click sequence still works.
