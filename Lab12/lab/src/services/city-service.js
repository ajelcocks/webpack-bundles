import "whatwg-fetch";

export default class CityService {
  static get() {
    return fetch("api/cities", {
      method: "GET"
    })
      .then(response => response.json())
      .catch(function(error) {
        console.log(error);
      });
  }
}
