# Prerequisites

```
npm install
```

# JSDoc

This section covers JSDoc in VSCode, so kill all terminals and debug sessions currently open since they are not needed.

You will need a terminal open at the projects lab folder to install things.

## Configuring ESLint JSDoc

```
npm install --save-dev jsdoc eslint-plugin-jsdoc eslint-plugin-require-jsdoc eslint-loader
```

Update `.eslintrc.js` to add a plugins section before the rules, with `jsdoc` as the value.

````json
  plugins: ["jsdoc"],
,,,

 Add associated rules to the rules section to validate if specified items have jsdoc

```json
    "require-jsdoc": [
      "error",
      {
        "require": {
          "FunctionDeclaration": true,
          "MethodDefinition": true,
          "ClassDeclaration": true,
          "ArrowFunctionExpression": true,
          "FunctionExpression": true
        }
      }
    ],
    "valid-jsdoc": 1,
    "jsdoc/check-param-names": 1,
    "jsdoc/check-tag-names": 1,
    "jsdoc/check-types": 1,
    "jsdoc/newline-after-description": 1,
    "jsdoc/no-undefined-types": 1,
    "jsdoc/require-description": 1,
    "jsdoc/require-description-complete-sentence": 1,
    "jsdoc/require-example": 0,
    "jsdoc/require-hyphen-before-param-description": 1,
    "jsdoc/require-param": 1,
    "jsdoc/require-param-description": 1,
    "jsdoc/require-param-name": 1,
    "jsdoc/require-param-type": 1,
    "jsdoc/require-returns-description": 1,
    "jsdoc/require-returns-type": 1,
    "jsdoc/valid-types": 1
````

## Run eslint on build

in `webpack.config.js` replace...

```json
  loader: "babel-loader",
```

...with a `use` item....

```json
use: ["babel-loader", "eslint-loader"],
```

as seen below...

```javascript
module.exports = {

  module: {
    rules: [
      {
        test: /\.js$/,
        use: ["babel-loader", "eslint-loader"],
        exclude: /node_modules/
```

```javascript
/*
Order is important. We need eslint-loader to run before babel-loader (they are invoked in reverse order).
*/
```

## Configuring JSDoc

Add a `script` entry to `package.json` to generate the entire doc.

```json
"jsdoc": "jsdoc -r --destination jsdoc -c jsdoc.conf ./src"
```

Save the changes.

Output is created in `out` directory by default.
Use the `--destination (-d)` option to specify another directory.

Use `-r` to specifiy directory recursion

Use -c [filename] to specify a config file

Need to use the `jsdoc-export-default-interop` plugin so that `export default class` is processed correctly

```
npm install --save-dev jsdoc-export-default-interop
```

Create a file named `jsdoc.conf` in root directory

```json
{
  "plugins": ["./node_modules/jsdoc-export-default-interop/dist/index"]
}
```

add the `jsdoc` folder to `.gitignore`

## Generating JSDoc

Genrating JSDoc for the project:

```
npm run jsdoc
```

Generate jsdoc for an individual file.

```
./node_modules/.bin/jsdoc yourJavaScriptFile.js
```

## Generating JSDoc errors

There is nothing in it at the moment. However...

```
npm run eslint
```

This will yield errors.

Opening a file will also generate errors for that file since eslint validates each file as it is opened in the editor.

## Correcting JSDoc errors.

Open `app.js` either from the file explorer or by clicking the link in the terminal.

Hover over the indicated error in the code to see what is required, or click the link in terminal.

Correct the errors

Re-Run jsdoc

# reference
see http://usejsdoc.org/howto-es2015-classes.html
