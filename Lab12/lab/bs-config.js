module.exports = {
  server: "./dist",
  port: 3014,
  open: false,
  ui: false,
  watchOptions: {
    ignored: "test"
  }
};
