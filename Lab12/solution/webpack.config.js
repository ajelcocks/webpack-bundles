const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const InlineSourcePlugin = require("html-webpack-inline-source-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const pathsToClean = ["dist"];
const cleanOptions = {
  root: __dirname,
  verbose: false,
  watch: false
};

module.exports = {
  devtool: "eval-source-map",
  entry: {
    index: "./src/index.js"
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].[chunkhash].js"
  },
  watchOptions: {
    ignored: /test/
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          name: "vendor",
          test: /node_modules/,
          chunks: "initial",
          minChunks: 1
        }
      }
    },
    runtimeChunk: {
      name: "manifest"
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Simple project",
      template: "./src/index.ejs",
      minify: {
        collapseWhitespace: true
      },
      hash: true
    }),
    new InlineSourcePlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    }),
    new CleanWebpackPlugin(pathsToClean, cleanOptions)
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", MiniCssExtractPlugin.loader, "css-loader"]
      },
      {
        test: /\.scss$/,
        use: [
          "style-loader",
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader"
        ]
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "images/"
            }
          }
        ]
      },
      {
        test: /\.js$/,
        use: ["babel-loader", "eslint-loader"],
        exclude: /(node_modules)/
      },
      {
        test: /\.js$/,
        use: "source-map-loader",
        exclude: []
      }
    ]
  }
};
