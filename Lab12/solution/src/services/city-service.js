import "whatwg-fetch";
/** */
export default class CityService {
  /**
   * @example None
   * @returns {string} JSON containing the data for a given city.
   */
  static get() {
    return fetch("api/city", {
      method: "GET"
    })
      .then(response => response.json())
      .catch(function(error) {
        console.log(error);
      });
  }
}
