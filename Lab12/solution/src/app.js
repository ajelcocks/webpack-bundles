import { JL as logger } from "jsnlog";
import cityService from "./services/city-service";

/**
 * @class
 */
export default class App {
  /**
   * @description The constructor.
   */
  constructor() {
    this.sequence = 1;
    return this.buildUi();
  }

  /**
   * @description Build the UI.
   * @returns {undefined} Nothing.
   */
  buildUi() {
    let contents = document.createElement("div");

    contents.innerText = "This is an example PWA";

    const btn = document.createElement("button");
    btn.innerText = "Click me";
    contents.appendChild(btn);

    btn.onclick = this.doclick;
    return contents;
  }

  /**
   * @descritpion Invoked when the button is clicked.
   * @returns {undefined} Nothing.
   */
  doclick() {
    cityService
      .get()
      .then(data => {
        logger().log(3000, data);
      })
      .catch(function(error) {
        alert(error);
      });
  }
}
