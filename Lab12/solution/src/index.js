import "./app.css";
import "./app.scss";
import App from "./app.js";
import { JL as logger } from "jsnlog";

const uuid = "35F7416D-86F1-47FA-A9EC-547FFF510086";

logger.setOptions({
  defaultAjaxUrl: "/log",
  requestId: uuid
});

document.getElementById("contents").appendChild(new App());
