# Prerequisites

```
npm install
```

# Creating the initial app

```
npm install html-webpack-plugin --save-dev
```

open webpack.config.js and a require statement for html-webpack-plugin

```javascript
const HtmlWebpackPlugin = require("html-webpack-plugin");
```

Add a declaration for the plugin after the output item (before the last line `};`)

```javascript
plugins: [
  new HtmlWebpackPlugin({
    title: "Simple project",
    template: "./src/index.ejs",
    minify: {
      collapseWhitespace: true
    },
    hash: true
  })
];
```

Right-click the projects source folder and create a file named `index.ejs` with the following content:

```html
<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
  <title>
    <%= htmlWebpackPlugin.options.title %></title>
</head>

<body>
  Simple project
</body>

</html>
```

Now we need a way to serve the pages. For now we will use a simple dev server provided by webpack. Type the following into a terminal to install it.

```
npm install webpack-dev-server --save-dev
```

Now that it is installed we need to run it, so open `package.json` and add the following line to the scripts section:

```javascript
"start": "webpack-dev-server"
```

Now type `npm run start` in the terminal

Scroll the terminal to show http://localhost:8080, Ctrl+click or copy the link and open in Chrome

Open the console in chrome dev tools and see that the log statement was executed.

Update the index.ejs

```javascript
// Note the messages written to the console and see the page change`
```

Update the app.js - `// see the console and page change again`

```javascript
/*
Note that webpack-dev-server creates files in memory, so if you delete dist folder and update app.js the dist folder will not be created.

This is fine for simple proof-of-concepts but later we will explore a more permanent, robust server solution
*/
```

The server outputs some startup information to the console when it starts.

To fine tune it we can open `webpack.config.js` and add the following after the plugins section (after closing square bracket)

```javascript
devServer: {
  contentBase: path.join(__dirname + "dist"),
  compress: true,
  port: 8080,
  stats: "none",
  open: true
}
```

Stop the server (ctrl+c in the terminal) then restart it `npm start`
