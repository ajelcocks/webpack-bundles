const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: "./src/app.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "app.bundle.js"
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Simple project",
      template: "./src/index.ejs",
      minify: {
        collapseWhitespace: true
      },
      hash: true
    })
  ],
  devServer: {
    contentBase: path.join(__dirname + "dist"),
    compress: true,
    port: 8080,
    stats: "none",
    open: true
  }
};
