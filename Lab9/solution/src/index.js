import "./app.css";
import "./app.scss";

let contents = document.getElementById("contents");

contents.innerText = "This is a generic SPA";

const btn = document.createElement("button");
btn.innerText = "Click me";
contents.appendChild(btn);

btn.onclick = doclick;

function doclick() {
  alert("click");
}
