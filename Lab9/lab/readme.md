# Prerequisites

Kill any processes and close any terminals left from previous labs.

```
npm install
```

# Creating a generic project build

## Project setup

```
npm install
```

Delete logo.png and main.js

Remove the content from `app.css` and `app.scss`

Rename app.js to index.js and replace the contents with:

```javascript
import "./app.css";
import "./app.scss";

let contents = document.getElementById("contents");

contents.innerText = "This is a generic SPA";

const btn = document.createElement("button");
btn.innerText = "Click me";
contents.appendChild(btn);

btn.onclick = doclick;

function doclick() {
  alert("click");
}
```

change the `entry` item in webpack.config.js to:

```javascript
index: "./src/index.js";
```

## Enable debugging using source-maps

```
npm uninstall webpack-dev-server
npm install --save-dev lite-server source-map-loader html-webpack-inline-source-plugin
```

Add a constant to `webpack.config.js`

```javascript
const InlineSourcePlugin = require("html-webpack-inline-source-plugin");
```

Add the following to `webpack.config.js`:

```javascript
module.exports = {
  devtool: "eval-source-map",
```

Add the following to the `rules` section of `webpack.config.js`:

```javascript
      {
        test: /\.js$/,
        use: "source-map-loader",
        exclude: []
      }
```

Add the inlinesourceplugin to the plugins section

```javascript
new InlineSourcePlugin();
```

## Configuring the server

create a file named `bs-config.js` in the project root folder with the following content:

```javascript
module.exports = {
  server: "./dist",
  watchOptions: {
    ignored: "test"
  }
};
```

Change the `scripts` section in `package.json`

```json
  "scripts": {
    "build": "cross-env NODE_ENV=development webpack --watch",
    "start": "lite-server",
```

## Cache busting

Change `webpack.config.js` as follows:

Add `watch: false` to `cleanOptions`

Change the `filename` in the `output` section to use `chunkhash`

```javascript
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].[chunkhash].js"
```

Add a new section after `output`:

```javascript
  watchOptions: {
    ignored: /test/
  },
```

change `optimization.splitChunks.cacheGroups.commons` to:

```javascript
commons: {
  name: "vendor",
  test: /node_modules/,
  chunks: "initial",
  minChunks: 1
```

add a runtimeCheck item to the `optimization` section (after `splitChunks`)

```javascript
runtimeChunk: {
  name: "manifest";
}
```

Open a terminal to the project folder

```
npm run build
```

open another terminal to the project folder

```
npm run start
```

```javascript
/*
 * Note how two items are in the terminal selection drop down.
 * 
 * 1: node is running the build watching process
 * 2: bash is running the the local webserver for delivering the client
 */
```

1: Show how changing the code re-compiles and creates new bundle

2: Note how clean is only executed when `npm run build` is executed
You must kill the build process (1: node) not the client (2: bash)

3: Note how adding a change to code and then reverting that change creates a new file for the change but not the revert, because the hashed file is already present.
