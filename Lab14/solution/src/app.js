import weatherService from "./services/weather-service";
import Card from "./card";
import i18next from "i18next";
import Toolbar from "./toolbar";
import { JL as logger } from "jsnlog";

/**
 * Build the main interface
 */
export default class App {
  /**
   * Initialize the application.
   *
   * @example description
   * @class
   */
  constructor() {
    this.visibleCards = {};
    this.selectedCities = [];
    this.container = document.querySelector(".main");
    this.toolbar = new Toolbar();

    this.addHandlers();
    this.addServiceWorker();

    this.init();
  }

  /**
   * Initialize the button handlers.
   *
   * @example Description.
   * @returns {undefined} Nothing.
   */
  addHandlers() {
    this.toolbar.addEventListener(Toolbar.event.REFRESH, () => {
      this.updateForecasts();
    });

    this.toolbar.addEventListener(Toolbar.event.ADD, item => {
      this.addCity(item);
    });

    i18next.on("languageChanged", () => {
      if (i18next.language) {
        this.translate();
      }
    });
  }

  /**
   * Iterate all of the cards and attempt to get the latest forecast data.
   *
   * @example Nothing.
   * @returns {undefined} Nothing.
   */
  updateForecasts() {
    var keys = Object.keys(this.visibleCards);
    keys.forEach(key => {
      this.getForecast(key);
    });
  }

  /**
   * @example None.
   * @returns {undefined} Nothing.
   */
  addCity() {
    // 1. Get the selected item from the toolbar
    let select = this.toolbar.getSelect();
    let selected = select.options[select.selectedIndex];
    let key = selected.value;
    let label = selected.textContent;

    // 2. Get the forcast
    this.getForecast(key, label);

    // 3. selectedCities is an index of the cards. Push the card key and label onto the index.
    this.selectedCities.push({
      key: key,
      label: label
    });

    // 4. Save the list of currently selected this.selectedCities.
    this.saveSelectedCities();
  }

  /**
   * Gets a forecast for a specific city and updates the card with the data.
   * First checks if the weather data is in the cache. If so,
   * then it gets that data and populates the card with the cached data.
   * Then it goes to the network for fresh data. If the network
   * request goes through, the card gets updated a second time with the
   * freshest data.
   *
   * @example Description.
   * @param {string} key - The key used to cache the data.
   * @param {string} label - The name of the city.
   * @returns {undefined} Nothing.
   */
  getForecast(key, label) {
    var url = weatherService.getUrl(key);

    if ("caches" in window) {
      /*
       * Check if the service worker has already cached this city's weather
       * data. If the service worker has the data, then display the cached
       * data while the app fetches the latest data.
       */
      caches.match(url).then(function(response) {
        if (response) {
          response.json().then(function updateFromCache(json) {
            var query = json.query;
            var results = query.results;
            results.key = key;
            results.label = label;
            results.created = json.query.created;
            this.updateForecastCard(results);
          });
        }
      });
    }

    weatherService.getForecast(key).then(query => {
      var results = query.results;
      results.key = key;
      results.label = label;
      results.created = query.created;
      this.updateForecastCard(results);
    });
  }

  /**
   * Updates a weather card with the latest weather forecast. If the card
   * doesn't already exist, it's cloned from the template.
   *
   * @example Description.
   *
   * @param {Object} data - The data used to populate the card.
   * @returns {undefined} Nothing.
   */
  updateForecastCard(data) {
    var card = this.visibleCards[data.key];
    if (!card) {
      card = new Card(data);

      // Add a close handler to the card so we can remove it from the index.
      card.addCloseHandler(() => {
        this.closeCard(card);
      });

      // Add the card to the UI and the card collection.
      this.container.appendChild(card.getElement());
      this.visibleCards[data.key] = card;
    }

    card.update(data);
  }

  /**
   * Save the currently selected cities to local storage.
   *
   * @example Nothing.
   * @returns {undefined} Nothing.
   */
  saveSelectedCities() {
    var selectedCities = JSON.stringify(this.selectedCities);
    localStorage.selectedCities = selectedCities;
  }

  /**
   * @example None.
   * @param {*} card - The card object to close.
   * @returns {undefined} Nothing.
   */
  closeCard(card) {
    let key = card.getKey();
    delete this.visibleCards[key];
    this.selectedCities = this.selectedCities.filter(x => x.key != key);
    this.saveSelectedCities();
    card.close();
  }

  /**
   * @example None.
   * @returns {undefined} Nothing.
   */
  translate() {
    for (let key in this.visibleCards) {
      let item = this.visibleCards[key];
      item.updateLabels();
    }
    let nodes = document.querySelectorAll("[i18n]");
    for (let ele of nodes) {
      let i18n = ele.getAttribute("i18n");
      ele.innerText = i18next.t("t:" + i18n);
    }
  }

  /**
   * Initialize the UI with any cards saved in local storage.
   *
   * @example Nothing.
   * @returns {undefined} Nothing.
   */
  init() {
    this.selectedCities = localStorage.selectedCities;
    if (this.selectedCities) {
      this.selectedCities = JSON.parse(this.selectedCities);
      this.selectedCities.forEach(city => {
        this.getForecast(city.key, city.label);
      });
    }
  }

  /**
   * @example None.
   * @returns {undefined} Nothing.
   */
  addServiceWorker() {
    if ("serviceWorker" in navigator) {
      logger().info("Service Worker available");
      /*
      AJE: Service worker breaks browsersync in chrome desktop as of August 2018
      */
      navigator.serviceWorker.register("./service-worker.js").then(function() {
        logger().info("Service Worker Registered");
      });
      /**/
    }
  }
}
