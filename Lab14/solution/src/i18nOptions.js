export let i18nOptions = {
  fallbackLng: "en",
  returnObjects: true,
  debug: false,
  ns: ["t"],
  backend: { loadPath: "/locales/{{lng}}/{{ns}}.json", crossDomain: true },
  interpolation: {
    /**
     * @example Description.
     * @param {string} value - The value to be formatted.
     * @param {string} format - The regex format.
     * @returns {undefined} Nothing.
     */
    format: function(value, format) {
      if (format === "uppercase") {
        return value.toUpperCase();
      }
      return value;
    }
  }
};
