import weatherCodes from "./weather-codes";
import * as constants from "./constants";

/**
 * Class rendering the current weather details.
 */
export default class Current {
  /**
   * @example None.
   */
  constructor() {
    this.node = document.querySelector(".currentTemplate").cloneNode(true);
    this.node.classList.remove(".currentTemplate");
    this.node.removeAttribute(constants.attr.HIDDEN);

    this.icon = this.node.querySelector(".current .icon");
    this.temperature = this.node.querySelector(".current .temperature .value");
    this.sunrise = this.node.querySelector(".current .sunrise");
    this.sunset = this.node.querySelector(".current .sunset");
    this.humidity = this.node.querySelector(".current .humidity");
    this.wind = this.node.querySelector(".current .wind .value");
    this.direction = this.node.querySelector(".current .wind .direction");
  }

  /**
   * @example None.
   * @param {*} data - The data.
   * @returns {undefined} Nothing.
   */
  update(data) {
    this.data = data;
    this.icon.classList.add(
      weatherCodes.getIconClass(data.item.condition.code)
    );
    this.temperature.textContent = Math.round(data.item.condition.temp);
    this.sunrise.textContent = data.astronomy.sunrise;
    this.sunset.textContent = data.astronomy.sunset;
    this.humidity.textContent = Math.round(data.atmosphere.humidity) + "%";
    this.wind.textContent = Math.round(data.wind.speed);
    this.direction.textContent = data.wind.direction;
  }

  /**
   * @example None.
   * @returns {undefined} Nothing.
   */
  getElement() {
    return this.node;
  }
}
