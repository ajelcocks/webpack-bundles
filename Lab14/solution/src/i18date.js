import i18next from "i18next";

let format = Object.freeze({
  LONG: 1,
  MEDIUM: 2,
  SHORT: 3,
  MEDIUM_DT: 4,
  LONG_DT: 5
});

let options = {
  // Thursday, August 2, 2018
  LONG: {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric"
  },
  // Thursday, August 2, 2018
  LONG_DT: {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
    timeZoneName: "short"
  }
};

/**
 * @example None.
 * @param {Date} date - A UTC date.
 * @param {*} format - The format to be used.
 * @param {boolean} military - True if 24 hour time format should be used.
 * @returns {string} The formatted date.
 */
function formatDate(date, format, military) {
  var opt = {
    timeZoneName: "short",
    hour12: !military
  };

  switch (format) {
    case undefined:
    case this.format.LONG:
      opt = options.LONG_OPTIONS;
      break;
    case this.format.MEDIUM:
    case this.format.SHORT:
    case this.format.MEDIUM_DT:
    case this.format.LONG_DT:
      opt = options.LONG_DT;
      break;
    default:
      opt = options.LONG_OPTIONS;
  }

  if (this.zone) {
    opt.timeZone = this.zone;
  }

  let lang = i18next.language;
  return new Intl.DateTimeFormat(lang ? lang : "en-US", opt).format(date);
}

/**
 * Provides the ability to set the timeone used to convert dates.
 * Accepts timezone values as defined in the {@link https://www.iana.org/time-zones|iana database}.
 * See also {@link https://en.wikipedia.org/wiki/List_of_tz_database_time_zones|Wikipedia}.
 *
 * @example i18date.setTimeZone("America/Los_Angeles");
 * @param {string} timezone - The timezone string.
 * @returns {undefined} Nothing.
 */
function setTimeZone(timezone) {
  this.zone = timezone;
}

module.exports = {
  format,
  formatDate,
  setTimeZone
};
