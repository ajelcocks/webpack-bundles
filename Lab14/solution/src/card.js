import weatherCodes from "./weather-codes";
import i18next from "i18next";
import { attr, event } from "./constants";
import Forecast from "./forecast";
import Current from "./current";
import i18date from "./i18date";

/**
 * Class representing a single forcast card
 */
export default class Card {
  /**
   * @example None.
   *
   * @param {*} data - The data.
   */
  constructor(data) {
    this.key = data.key;

    this.card = document.querySelector(".cardTemplate").cloneNode(true);
    this.card.classList.remove("cardTemplate");
    this.card.removeAttribute(attr.HIDDEN);
    this.card.querySelector(".location").textContent = data.label;

    this.current = new Current();
    this.forecast = new Forecast();
    this.card.appendChild(this.current.getElement());
    this.card.appendChild(this.forecast.getElement());

    this.label = data.label;
    this.update(data);

    return this;
  }

  /**
   * @example Example.
   * @param {*} data - The data object used to update this card.
   * @returns {undefined} Nothing.
   */
  update(data) {
    this.data = data;
    let dataLastUpdated = new Date(data.created);
    let current = data.channel.item.condition;

    // Verifies the data provided is newer than what's already visible
    // on the card, if it's not bail, if it is, continue and update the
    // time saved in the card
    var cardLastUpdatedElem = this.card.querySelector(".card-last-updated");
    var cardLastUpdated = cardLastUpdatedElem.textContent;
    if (cardLastUpdated) {
      cardLastUpdated = new Date(cardLastUpdated);
      // Bail if the card has more recent data then the data
      if (dataLastUpdated.getTime() < cardLastUpdated.getTime()) {
        return;
      }
    }
    cardLastUpdatedElem.textContent = data.created;

    let icon = weatherCodes.getIconClass(current.code);
    let desc = i18next.t(`t:${icon}`);
    this.card.querySelector(".description").textContent = desc
      ? desc
      : current.text;

    let fmt = i18date.format.LONG_DT;
    this.card.querySelector(".date").textContent = i18date.formatDate(
      new Date(current.date),
      fmt
    );

    this.current.update(this.data.channel);
    this.forecast.update(this.data.channel.item.forecast);
  }

  /**
   * @example Example.
   * @returns {undefined} Nothing.
   */
  updateLabels() {
    this.update(this.data);
  }

  /**
   * @example Example.
   * @param {Function} handler - The callback for the click event.
   * @returns {undefined} Nothing.
   */
  addCloseHandler(handler) {
    this.card
      .querySelector(".card-close")
      .addEventListener(event.CLICK, handler);
  }

  /**
   * Get the key used to retrieve the dtata populating this card.
   *
   * @example Example.
   * @returns {number} The unique key for this card.
   */
  getKey() {
    return this.key;
  }

  /**
   * @example Example.
   * @returns {HTMLElement} The HTMLElement redering this card.
   */
  getElement() {
    return this.card;
  }

  /**
   * @example Example.
   * @returns {string} The label (city name) for this card.
   */
  getLabel() {
    return this.label;
  }

  /**
   * Close the card, removing it from the DOM.
   *
   * @example None.
   * @returns {undefined} Nothing.
   */
  close() {
    if (this.card.remove) {
      this.card.remove();
    } else {
      this.card.parentElement.removeChild(this.card);
    }
    delete this.card;
  }
}
