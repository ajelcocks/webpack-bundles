# Prerequisites

Kill any processes and close any terminals left from previous labs.

```
npm install
```

# Aknowledgements

This is a highly modified version of the [Google service worker tutorial](https://developers.google.com/web/fundamentals/codelabs/your-first-pwapp/)


Note:
The requirement for @example in jsdoc has been removed in `.eslintrc.js` 
```json
    "jsdoc/require-example": 0,
```



# Setting up the dev environment

1. Open a terminal in the middleware project
    Run the proxy and logger using 
```
npm run all
```
OR use startup.bat in windows.

2. Open an external terminal in the lab folder
```
npm install
npm run all
```

3. make `launch.json` in `.vscode` folder like this...
```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "type": "chrome",
      "request": "launch",
      "name": "Chrome local Lab14",
      "url": "http://localhost:3010",
      "webRoot": "${workspaceFolder}/Lab14/lab"
    }
  ]
}
```

4. Select Chrome Lab from the debug launch bar
This will open the application in Chrome

5. Click the button to confirm the UI is working.
Change the title in `locales/en/t.jsn to confirm BrowserSync is attached.
Change it back! (undo)






# Design

Consider the following wireframes found in the `wireframes` folder of the lab:

![The app wireframe](wireframes/app.png)

![The city dialog](wireframes/dialog.png)

If we deconstruct the app we can see (one way) to create objects as follows:

1. A toolbar, an area to contain one or more weather cards and a city dialog
    1. the Toolbar item has buttons for refresh, add forecast, and a menu donut.
    2. Hint: The donut provides language selection (french/english)

2. Each card has a summary with a close button, todays weather, and a forecast.

Open `component-map.html` in a browser (found in the wireframes folder) to see the component map.






# Toolbar

The `template` attribute of `HtmlWebpackConfig` in `webpack.config.js` has been changed to use ejs templates

```javascript
    new HtmlWebpackPlugin({
      title: "Weather PWA",
      template: "!!ejs-compiled-loader!./src/index.ejs",
```



# Add the content of the app

open `index.ejs` and change the body:
```html
<body>
  <%- include src/views/toolbar %>
  <%- include src/views/main %>
</body>
```

Save and review the ui

1. `index.ejs`
    1. include `toolbar.ejs`

        1. Defines html for the toolbar
        2. include `dialog.ejs`

    2. include `main.ejs`

        1. include `summary.ejs`

            This renders the name, time and current condition (cloudy, sunny, etc.)

        2. include current

            This shows a pictorial representation of the current condition, the temperature and several itemized statistics

        3. include future

            A seven day forcast
        



# Enable the code to handle the button clicks in toolbar

open `toolbar.js` and remove
1. the comment from the import for `i18next`
2. the invocation of the menu handlers in the constructor.
3. Review the code

Click the tool button to confirm they operate correctly.

## Review
1. `toolbar.ejs`

    Defines the buttons, the language selector and contains an include for the city dialog.

    Note the include is relative to the path of the item containing the include.

2. `dialog.ejs`

    Defines the html used to render the city dialog
    Contains a selct element with a list of city names and the buttons referenced in the Toolbar class constructor.

3. `toolbar.js`

    The constructor establishes references to the various components it will interact with and attaches various handlers to those objects to react when an action is performed.

    Open `toolbar.js` and review the code






# Adding a card
1. enable the handlers in `app.js`

    remove the comments from code in the `addHandlers()` function (line 33)

2. set a break on the first line of the `addCity` function (line 67)

3. Click the 'Add city' button in the toolbar

4. Click the 'Add' button in the dialog

    The debugger should activate at the break point.

5. Once the card is created
    1. remove the break point
    2. open the developer tools (win: ctrl+shift+i) or (mac: cmd+option+i)
    3. select the applications tab
    4. expand the `Storage > local storage` item of the left hand panel




# Rembering state

1. Refresh the browser.

    The card is lost.

2. Remove the comment from the code of the `init` methods in `app.js`

    Review the code.

    Save the file and the browser will refresh. This time the card is preserved.

3. Delete the card

4. Open browser tools.

    Note that the storage is now an empty array.

    The `closeCard` method invokes `saveSelectedCities()` which overwrites with the new (empty) array.





# Internationalization

Click the donut in the toolbar and select the (other) language

This works as in the previous lab. The event is detected in app.js which iterates over the card collection and invokes the translate function.





# Plugins have been added to `webpack.config.js`

```javascript
    new SWPrecacheWebpackPlugin({
      minify: true
    }),
    new WebpackPwaManifest({
      name: "Weather PWA",
      short_name: "PWA",
      description: "Weather Progressive Web App!",
      background_color: "#ffffff",
      inject: "true",
      ios: "true",
      icons: [
        {
          src: path.resolve("weather-pwa.png"),
          sizes: [36, 48, 72, 96, 128, 144, 192, 256, 384, 512],
          destination: path.join("images", "icons")
        }
      ]
    })
```



# Add to Home Screen

see [this page] (https://developers.google.com/web/fundamentals/app-install-banners/) for more info.

`WebpackPwaManifest` generates a `manifest.json` with auto icon resizing and fingerprinting support.

Note that `HtmlWebpackPlugin` must be before `WebpackPwaManifest` in the plugins array.

The manifest is a JSON file that tells the browser about the web application and how it should behave when 'installed' on the users mobile device or desktop.

A typical manifest file includes information about the app name, icons it should use and the url it should start at when launched.

`WebpackPwaManifest` also generates the varios icons required by the various mobile devices using a image specified in the configuration. In this case that is `weather-pwa.png`



## Windows tiles

For IE 8+

```xml
<meta name="msapplication-TileColor" content="#D83434">
<meta name="msapplication-TileImage" content="path/to/tileicon.png">
```

The first line is the tile color, you can use a hex value, RGB or a CSS color name.
The second line is the path to the icon.
For best results the icon should be transparent and sized at 144x144 pixels.

IE 11+ on Windows 8.1+ does offer a way to create pinned tiles for your site.

Microsoft recommends creating a few tiles at the following size:

* Small: 128 x 128
* Medium: 270 x 270
* Wide: 558 x 270
* Large: 558 x 558

These should be transparent images as we will define a color background next.

Once these images are created you should create an xml file called browserconfig.xml with the following code:

```xml
<?xml version="1.0" encoding="utf-8"?>
<browserconfig>
  <msapplication>
    <tile>
      <square70x70logo src="images/smalltile.png"/>
      <square150x150logo src="images/mediumtile.png"/>
      <wide310x150logo src="images/widetile.png"/>
      <square310x310logo src="images/largetile.png"/>
      <TileColor>#009900</TileColor>
    </tile>
  </msapplication>
</browserconfig>
```

Save this xml file in the root of your site. When a site is pinned IE will look for this file.
For additional information on IE 11+ custom tiles and using the XML file visit Microsoft's website.

https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/samples/dn455106(v=vs.85)






# Service workers

`SWPrecacheWebpackPlugin` generates (the code for) a service worker and adds it to the project.

A service worker will run when the domain used is `localhost` or when `https` is used.

We only need `https` when accessing the dev environment across the network, e.g. when connecting a mobile device to the dev machine.

Service workers in `iOS` currently (July 2018) only work in the `Safari` browser.



## Enabeling service worker reload

A service worker continues to control the page as long as there is a tab open to the page, so refreshing a page will cause a second worker to be created but not started.

Enable the `Update on Reload` checkbox on the `Service Worker` pane of `Chrome DevTools`.

Note this is domain specific, so the dev tools must be opened when the app is open in the browser.

There should currently be no service worker listed in dev tools.



## Register a service worker

In order to use a service worker the application code must check if the browser supports service workers, and if it does, register the service worker.

Open `app.js` and remove the comments from the `addServiceWorker()` method.


1. Save the change.

2. Open the chrome dev tools

3. Selec tthe `application` tab

4. Click `Application > Service worker` in the left-hand panel.
    There should now be one present.

5. Click `Cache > Cache storage > swPrec...`
    Note all the files are cahced.

6. kill the app server (ctrl+c in the terminal)

7. Refresh the browser.
    See that the app still loaded, but css is missing.

8. Select the elements tab in Chrome dev tools and expand the `head` section in the html

    Look at the link for the css. There is a hash at the end as a query parameter.

9. Look at the file in the cache.
    It has no query param. The file caching ignores any query parameters, so it must be removed from the generated file.

10. Open `webpack.config.js` and find the `HTMLWebpackPlugin`
    Change the hash attribute to false.

11. Restart the app server.

12. Refresh the browser
    We need to restart so we can update the cache.

13. kill the app server

14. Refresh the browser.
    The application still renders and functions. In this case the weather will also be updated because we only killed our server, the yaho weather service is still responding. If there were no internet connection then the refresh would have no effect.


Note that service workers break BrowserSync in chrome desktop as of August 2018



## Testing on mobile devices.

Mobile devices can be tested across the network by using the network name of the computer (with VS Code) running the application server.

The proxy server started by the `middleware` project opens ports for both http and https. A certificate has been created for netscript.

In order for the certificate to be trusted the signing authority must be recognized by the mobile device.

Look at the readme.md file in the middleware project for details.






# Modules, Classes and 'immutable' items

A class can be instantiated while a module cannot.

Class member functions and variables are publicly accessible while they are not visible in modules unless explicity stated.

Modules are useful as singletons, utility `classes`, enums and constants.



# Module example

Open `constants.js`.

This is a module. The items in the file need to be `public` (accessible outside the module) and so are declared with the `export` keyword.

An alternative would be to place code at the end of the file:
```javascript
 modules.exports = {
     attr,
     event
 }
 ```

Notice the json does not use the familiar `"key":"value"` syntax. This is a new abbreviation in ES6 that allows the assignemnt to be omitted when the variable name is the same as the key.

Open `i18ndate.js` for an example.

# Class with enum

There is no syntactical definition for an enum but a json object can be used to emulate one.

Sometimes it is useful to provide an `enum` of values accepted by a class.
In this situation a static `enum` can declared on the class.

Similarly a staic method can also be declared.

Open `weather-codes.js` for an example.


# export default

export default can be used to declare the default item to be exported.
This is especially usefule for classes where it is usually only the class that needs to be exported.

This simplifies the import statement since the name ot the item need not be declared.



# import
import foo from "foo"
import {*} from "foo"
import {bar, baz} from foo;





# freeze

Object.freeze can be used to emulate, as much as possible, an immutable item.

Open `constants.js` for an example.