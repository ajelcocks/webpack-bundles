export const attr = Object.freeze({
  NONE: "none",
  INLINE_BLOCK: "inline-block",
  HIDDEN: "hidden"
});

export const event = Object.freeze({
  CLICK: "click",
  LANGUAGE_CHANGED: "languageChanged"
});
