import { event, attr } from "./constants";
//import i18next from "i18next";

/**
 * The Application toolbar bar.
 */
export default class Toolbar {
  /**
   * @example None.
   */
  constructor() {
    this.refresh = document.getElementById("butRefresh");
    this.add = document.getElementById("butAdd");
    this.addDialog = document.querySelector(".dialog-container");
    this.menu = document.getElementById("butMenu");
    this.menuContainer = document.querySelector(".menuContainer");
    this.addCity = document.getElementById("butAddCity");
    this.select = document.getElementById("selectCityToAdd");

    /*
    this.addMenuHandlers();
    this.addDialogHandlers();

    document.getElementById("selLanguage").addEventListener("change", () => {
      i18next.changeLanguage(document.getElementById("selLanguage").value);
      this.hideMenu();
    });
    */
  }

  /**
   * @example None.
   * @returns {undefined} A reference to the select object.
   */
  getSelect() {
    return this.select;
  }
  /**
   * Add the handlers for the (hamburger) menu.
   *
   * @example None.
   * @returns {undefined} Nothing.
   */
  addMenuHandlers() {
    this.menu.addEventListener(event.CLICK, () => {
      this.toggleMenu();
    });

    document.body.addEventListener(event.CLICK, evt => {
      this.checkMenu(evt);
    });
  }

  /**
   * Add the handlers for the dialog to add a city card.
   *
   * @example None.
   * @returns {undefined} Nothing.
   */
  addDialogHandlers() {
    this.add.addEventListener(event.CLICK, () => {
      this.showCityDialog(true);
    });

    this.addCity.addEventListener(event.CLICK, () => {
      this.showCityDialog(false);
    });

    document
      .getElementById("butAddCancel")
      .addEventListener(event.CLICK, () => {
        this.showCityDialog(false);
      });
  }

  /**
   * Invoked when an event occurs in the UI.The code checks if that event was
   * fired by a node in the language selector and, if not, closes the selector.
   *
   * @example this.checkMenu(event).
   * @returns {undefined} Nothing.
   * @param {Event} evt - The event.
   */
  checkMenu(evt) {
    if (
      !this.menuContainer.contains(evt.srcElement) &&
      evt.srcElement != this.menu
    ) {
      this.hideMenu();
    }
  }
  /**
   * Hide the language selection menu.
   *
   * @example this.hideMenu();
   * @returns {undefined} Nothing.
   */
  hideMenu() {
    this.menuContainer.style.display = attr.NONE;
  }
  /**
   * Toggles the visibility of the language selector.
   *
   * @example Description.
   * @returns {undefined} Nothing.
   */
  toggleMenu() {
    let style = this.menuContainer.style;
    style.display =
      style.display == attr.INLINE_BLOCK ? attr.NONE : attr.INLINE_BLOCK;
  }

  /**
   * @example None.
   * @param {*} type - The type of handler being registered.
   * @param {Function} handler - The handler for the gievn type.
   * @returns {undefined} Nothing.
   */
  addEventListener(type, handler) {
    switch (type) {
      case Toolbar.event.REFRESH:
        this.refresh.addEventListener(event.CLICK, handler);
        break;
      case Toolbar.event.ADD:
        this.addCity.addEventListener(event.CLICK, handler);
        break;
    }
  }

  /**
   * Sets the visibiity of the City dialog.
   *
   * @example Description.
   * @param {boolean} visible - True if the dialog should be shown.
   * @returns {undefined} Nothing.
   */
  showCityDialog(visible) {
    if (visible) {
      this.addDialog.classList.add("dialog-container--visible");
      document.body.appendChild(this.addDialog);
    } else {
      this.addDialog.classList.remove("dialog-container--visible");
      document.body.removeChild(this.addDialog);
    }
  }
}

/**
 * enum used to attach handlers for a toolbar event.
 */
Toolbar.event = Object.freeze({
  REFRESH: 1,
  ADD: 2
});
