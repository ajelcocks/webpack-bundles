import weatherCodes from "./weather-codes";
import i18next from "i18next";
import { attr } from "./constants";

/**
 * Class representing the 7 day forecast.
 */
export default class Forecast {
  /**
   * @example None.
   */
  constructor() {
    this.forecast = document.querySelector(".futureTemplate").cloneNode(true);
    this.forecast.classList.remove(".futureTemplate");
    this.forecast.removeAttribute(attr.HIDDEN);
    return this;
  }

  /**
   * Update the forecast with the data provided.
   *
   * @example None.
   *
   * @param {*} data - The forecast data.
   * @returns {undefined} Nothing.
   */
  update(data) {
    var nextDays = this.forecast.querySelectorAll(".future .oneday");
    var today = new Date();
    today = today.getDay();
    for (var i = 0; i < 7; i++) {
      var nextDay = nextDays[i];
      var daily = data[i];
      if (daily && nextDay) {
        nextDay.querySelector(".date").textContent = i18next.t(
          "t:days-of-week"
        )[(i + today) % 7];
        nextDay
          .querySelector(".icon")
          .classList.add(weatherCodes.getIconClass(daily.code));
        nextDay.querySelector(".temp-high .value").textContent = Math.round(
          daily.high
        );
        nextDay.querySelector(".temp-low .value").textContent = Math.round(
          daily.low
        );
      }
    }
  }

  /**
   * Get the DOM element for this forecast.
   *
   * @example None.
   * @returns {HTMLElement} The DOM element for this forecast.
   */
  getElement() {
    return this.forecast;
  }
}
