module.exports = {
  server: "./dist",
  port: 3012,
  open: false,
  ui: false,
  watchOptions: {
    ignored: "test"
  }
};
