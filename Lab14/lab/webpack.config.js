const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const InlineSourcePlugin = require("html-webpack-inline-source-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const SWPrecacheWebpackPlugin = require("sw-precache-webpack-plugin");
const WebpackPwaManifest = require("webpack-pwa-manifest");
const pathsToClean = ["dist"];
const cleanOptions = {
  root: __dirname,
  verbose: false,
  watch: false
};

module.exports = {
  devtool: "eval-source-map",
  entry: {
    index: "./src/index.js"
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].[chunkhash].js"
  },
  watchOptions: {
    ignored: /test/
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          name: "vendor",
          test: /node_modules/,
          chunks: "initial",
          minChunks: 1
        }
      }
    },
    runtimeChunk: {
      name: "manifest"
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Weather PWA",
      template: "!!ejs-compiled-loader!./src/index.ejs",
      minify: {
        collapseWhitespace: true
      },
      hash: true
    }),
    new InlineSourcePlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].[hash].css",
      chunkFilename: "[id].[hash].css"
    }),
    new CleanWebpackPlugin(pathsToClean, cleanOptions),
    new CopyWebpackPlugin([
      {
        from: "images",
        to: "images"
      },
      {
        from: "locales",
        to: "locales"
      }
    ]),
    new SWPrecacheWebpackPlugin({
      minify: true
    }),
    new WebpackPwaManifest({
      name: "Weather PWA",
      short_name: "PWA",
      description: "Weather Progressive Web App!",
      background_color: "#ffffff",
      inject: "true",
      ios: "true",
      icons: [
        {
          src: path.resolve("weather-pwa.png"),
          sizes: [36, 48, 72, 96, 128, 144, 192, 256, 384, 512],
          destination: path.join("images", "icons")
        }
      ]
    })
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", MiniCssExtractPlugin.loader, "css-loader"]
      },
      {
        test: /\.scss$/,
        use: [
          "style-loader",
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader"
        ]
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "images/"
            }
          }
        ]
      },
      {
        test: /\.js$/,
        use: ["babel-loader", "eslint-loader"],
        exclude: /(node_modules)/
      },
      {
        test: /\.js$/,
        use: "source-map-loader",
        exclude: []
      }
    ]
  }
};
