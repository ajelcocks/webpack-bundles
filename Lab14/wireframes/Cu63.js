﻿(function(window) {
  /**
   * @example None.
   * @param {*} window - A refernece to the browser window object.
   * @returns {undefined} Nothing.
   */
  function Cu63(window) {
    // Pollute the ecosphere!
    if (typeof window.String.prototype.startsWith != "function") {
      window.String.prototype.startsWith = function(str, ignoreCase) {
        if (ignoreCase) {
          return str && str.length <= this.length
            ? this.substring(0, str.length).toLowerCase() === str.toLowerCase()
            : false;
        } else {
          return str && str.length <= this.length
            ? this.substring(0, str.length) === str
            : false;
        }
      };
    }
    if (typeof window.String.prototype.endsWith != "function") {
      window.String.prototype.endsWith = function(str, ignoreCase) {
        if (ignoreCase) {
          return str && str.length <= this.length
            ? this.substring(this.length - str.length).toLowerCase() ===
                str.toLowerCase()
            : false;
        } else {
          return str && str.length <= this.length
            ? this.substring(this.length - str.length) === str
            : false;
        }
      };
    }

    var CLASSNAME = {
      boxes: "boxes",
      caption: "caption",
      contextMenu: "contextMenu",
      contract: "contract",
      details: "details",
      dividor: "dividor",
      drag: "drag",
      ide: "ide",
      menuItem: "menuItem",
      navtree: "navtree",
      noide: "noide",
      node: "node",
      threed: "threed"
    };

    var STYLE = {
      crsDefault: "default",
      crsMove: "move",
      none: "none",
      block: "block"
    };

    var KEYCODE = { ENTER: 13, ESC: 27 };

    var EVENT = {
      blur: "blur",
      click: "click",
      contextmenu: "contextmenu",
      keydown: "keydown",
      load: "load",
      mouseover: "mouseover",
      mouseout: "mouseout",
      paste: "paste"
    };

    var UNIT = { px: "px" };

    var CONSTANT = {
      doubleRight: "&gt;&gt;",
      doubleLeft: "&lt;&lt;",
      chartNode: "chartNode_",
      zoom: "zoom",
      empty: "",
      invalidCuDoc: "Invalid Copper document.",
      space: " ",
      strTrue: "true",
      strFalse: "false",
      newNode: "New Node",
      titleEnd: "title>"
    };

    var Tag = {
      BUTTON: "BUTTON",
      DIV: "DIV",
      HTML: "HTML",
      INPUT: "INPUT",
      LABEL: "LABEL",
      LI: "LI",
      META: "META",
      SECTION: "section",
      SPAN: "SPAN",
      UL: "UL"
    };
    // Make Tag public
    this.Tag = Tag;

    var ATTRIBUTE = { tabindex: "tabindex" };

    var nodeId = 1;
    var zoomLevel = 70;
    var zoomPercent;
    var contextMenu, optionsTrigger, optionsMenu, applicationMenu, appBody;
    var componentMap, navtree;
    var chartDragger;

    var plugins = new Array();

    /*
	   * ======================
	   * public functions (API)
	   * ======================
	   */

    this.addMenuItem = function(title, func) {
      applicationMenu.appendChild(new MenuItem(title, func).getElement());
    };

    this.createMenu = function() {
      return new Cu63Menu(CLASSNAME.contextMenu);
    };

    this.createNode = function() {
      return new ChartNode();
    };

    this.register = function(obj) {
      plugins.push(obj);
      obj.load(this);
    };

    this.loadChart = function(content) {
      loadChart(content);
    };
    this.getContextMenu = function() {
      return contextMenu;
    };
    this.getChart = function() {
      return componentMap.getChart();
    };
    this.getNavTree = function() {
      return navtree;
    };
    this.refreshNavTree = function() {
      return createNavTree();
    };
    this.newMap = function() {
      newMap(new ComponentMap());
      //        editNode(cuMap.getRootNode().getElement());
    };

    /*
     * =================
     * private functions
     * =================
     */

    /**
     * @example None.
     * @returns {undefined} Nothing.
     */
    function onLoad() {
      applicationMenu = createAppMenu();
      appBody = document.getElementById("appBody");

      createContextMenu();

      // Do this last so that all objects are available to plugins
      var scripts = document.getElementsByTagName("script");
      for (var ix = 0; ix < scripts.length; ix++) {
        var src = scripts[ix].src;
        var sfx = "Cu63.js";
        if (src.indexOf(sfx, src.length - sfx.length) !== -1) {
          var attr = scripts[ix].getAttribute("plugins");
          if (attr) {
            var split = attr.split(",");
            for (var idx = 0; idx < split.length; idx++) {
              var scpt = document.createElement("script");
              scpt.src = split[idx];
              document.getElementsByTagName("head")[0].appendChild(scpt);
            }
          }
        }
      }

      loadChart(document.documentElement.outerHTML);
    }

    /**
     * @example None.
     * @returns {undefined} Nothing.
     */
    function createAppMenu() {
      var menubar = document.getElementById("appToolbar");
      let appmenu = document.createElement(Tag.SPAN);
      menubar.appendChild(appmenu);

      // Must create the options menu before we can pass it to the app menu
      optionsMenu = new OptionsMenu();
      optionsTrigger = new MenuItem("Options", optionsMenu, false);
      menubar.appendChild(optionsTrigger.getElement());

      let zoombar = document.createElement(Tag.DIV);
      zoombar.className = "zoombar";
      zoombar.appendChild(
        new MenuItem(
          "-",
          function() {
            zoom(-1);
          },
          Tag.BUTTON
        ).getElement()
      );
      zoomPercent = document.createTextNode(zoomLevel + "%");
      zoombar.appendChild(zoomPercent);
      zoombar.appendChild(
        new MenuItem(
          "+",
          function() {
            zoom(1);
          },
          Tag.BUTTON
        ).getElement()
      );
      menubar.appendChild(zoombar);

      return appmenu;
    }

    /**
     * @example None.
     * @param {string} content - HTML as a string.
     * @returns {undefined} Nothing.
     */
    function loadChart(content) {
      /*
         * sanitize only returns the body content, so look for meta tags first
         * find the first meta tag <meta name="description" content="Cu63 Component Map">
         * <meta name="generator" content="Cu63">
         */
      var metadata = getMetadata(content.substring(0, 200));

      if (
        metadata &&
        metadata["description"] === "Copper Component Map Editor"
      ) {
        return;
      }

      if (
        !metadata ||
        !metadata["generator"] ||
        metadata["generator"] !== "Cu63"
      ) {
        return showError(CONSTANT.invalidCuDoc);
      }

      // eslint-disable-next-line no-undef
      var sanitized = html_sanitize(content);

      var foo;
      try {
        foo = new DOMParser().parseFromString(sanitized, "text/html");
      } catch (e) {
        //This must be Explorer
        return showError(e.code + ": " + e.message);
      }

      if (foo.firstChild.nodeName !== Tag.HTML) {
        return showError(CONSTANT.invalidCuDoc);
      }

      var parsererror = foo.getElementsByTagName("parsererror");
      if (parsererror && parsererror.length > 0) {
        return showError(parsererror.innerHTML);
      }

      let cuMap = new ComponentMap(foo);

      if (!cuMap) {
        return showError(CONSTANT.invalidCuDoc);
      }

      newMap(cuMap);
    }

    /**
     * @example None.
     * @param {string} msg - An error message.
     * @returns {undefined} Nothing.
     */
    function showError(msg) {
      appBody.innerHTML = msg;
    }

    /**
     * @example None.
     * @param {boolean} enable - True if the chart items should be rendered with data boxes.
     * @returns {undefined} Nothing.
     */
    function showBoxes(enable) {
      addStyle(
        componentMap.getChart(),
        enable ? CLASSNAME.boxes : null,
        enable ? null : CLASSNAME.boxes
      );
    }

    /**
     * @example None.
     * @param {boolean} enable - True if the boxes should be renderd with drop-shadows.
     * @returns {undefined} Nothing.
     */
    function showShadows(enable) {
      addStyle(
        componentMap.getChart(),
        enable ? CLASSNAME.threed : null,
        enable ? null : CLASSNAME.threed
      );
    }

    /**
     * @todo Dragging has not been implemented.
     *
     * @example None.
     * @param {boolean} enable - True if dragging of boxes should be allowed.
     * @returns {undefined} Nothing.
     */
    function enableDrag(enable) {
      chartDragger.enable(enable);
    }

    /**
     * @example None.
     * @param {string} content - The string representation of an HTML head element.
     * @returns {*} An object representing the metadata of a Copper document.
     */
    function getMetadata(content) {
      var metadata = {};
      var str = content.split("<");
      for (var idx = 0; idx < str.length; idx++) {
        var tag = str[idx];
        if (tag.toUpperCase().startsWith(Tag.META)) {
          parseMetaTag(tag.substring(Tag.META.length), metadata);
        } else if (tag.startsWith(CONSTANT.titleEnd)) {
          metadata.title = tag.substring(CONSTANT.titleEnd.length);
        }
      }
      return metadata;
    }

    /**
     * @example None.
     * @param {string} tag - A string representation of an HTML meta tag.
     * @param {*} metadata - An object containing key/value pairs extracted from metadata tags.
     * @returns {undefined} Nothing.
     */
    function parseMetaTag(tag, metadata) {
      // prettier-ignore
      var attr = tag.split("\"");
      var key, val;
      for (var ix = 0; ix < attr.length; ix++) {
        if (attr[ix].trim() === "name=") {
          key = attr[++ix].trim();
        } else if (attr[ix].trim() === "content=") {
          val = attr[++ix].trim();
          break;
        }
      }

      if (key && val) {
        metadata[key] = val;
      }
    }

    /**
     * @example None.
     * @param {number} incr - A number indicating the zoom level of the chart.
     * @returns {undefined} Nothing.
     */
    function zoom(incr) {
      var oldZoom = zoomLevel;
      var newZoom = oldZoom + incr * 10;
      if (newZoom > 150 || newZoom < 40) {
        return;
      }
      addStyle(
        componentMap.getChart(),
        CONSTANT.zoom + newZoom,
        CONSTANT.zoom + oldZoom
      );
      zoomLevel = newZoom;
      zoomPercent.nodeValue = zoomLevel + "%";
    }

    /**
     * @example None.
     * @returns {undefined} Nothing.
     */
    function createContextMenu() {
      contextMenu = new Cu63Menu();
      contextMenu.addItem("Contract", function() {
        contextMenu.hide();
        expandNode(contextMenu.getTarget());
      });
      contextMenu.addDividor();
    }

    /**
     * @example None.
     * @param {HTMLElement} obj - An HTML Element whose class names will be adjusted.
     * @param {string} add - A class name to be added.
     * @param {string} remove - A class name to be removed.
     * @returns {undefined} Nothing.
     */
    function addStyle(obj, add, remove) {
      if (add === remove) {
        return;
      }
      var names = obj.className.split(CONSTANT.space);
      var classnames = CONSTANT.empty;
      for (var idx = 0; idx < names.length; idx++) {
        if (
          !((remove && names[idx] === remove) || (names && names[idx] === add))
        ) {
          classnames +=
            (classnames === CONSTANT.empty ? CONSTANT.empty : CONSTANT.space) +
            names[idx];
        }
      }
      if (add) {
        classnames +=
          (classnames === CONSTANT.empty ? CONSTANT.empty : CONSTANT.space) +
          add;
      }
      obj.className = classnames;
    }

    /**
     * @example None.
     * @param {Event} evt - A paste event.
     * @returns {undefined} Nothing.
     */
    function onPaste(evt) {
      //if(isCaption(evt)) {
      evt.preventDefault();
      evt.stopPropagation();
      var clip = evt.clipboardData || evt.dataTransfer;
      var sel = window.getSelection();
      var range = sel.getRangeAt(0);
      range.setText(clip.getData("text"));
      //}
    }

    /**
     * @example None.
     * @param {Event} evt - An event object.
     * @returns {boolean} True if the given event was initiated against an HTML Element with a class name of caption.
     */
    /*
    function isCaption(evt) {
      var src = evt.target;
      if (src.className.indexOf(CLASSNAME.caption) >= 0) {
        return true;
      }
    }
    */

    /**
     * @example None.
     * @param {HTMLElement} src - An HTML object.
     * @returns {undefined} Nothing.
     */
    function editNode(src) {
      src.contentEditable = true;
      var s = window.getSelection(),
        r = document.createRange();
      r.selectNodeContents(src);
      s.removeAllRanges();
      s.addRange(r);

      /**
       * @example None.
       * @returns {undefined} Nothing.
       */
      var blurFunc = function() {
        src.contentEditable = false;
        src.removeEventListener(EVENT.blur, blurFunc, false);
        src.removeEventListener(EVENT.keydown, escFunc, false);
        createNavTree();
      };

      /**
       * Only call with KeyDown events because:
       *   Firefox codes are unpredictable onKeyPress.
       *   Cancel on keyDown prevents keyPress, but canceling keyUp does not.
       *
       * @example None.
       * @param {Event} evt - An event object.
       * @returns {undefined} Nothing.
       */
      var escFunc = function(evt) {
        if (evt.keyCode === KEYCODE.ENTER || evt.keyCode === KEYCODE.ESC) {
          blurFunc();
          evt.preventDefault();
          evt.stopPropagation();
        }
      };

      src.addEventListener(EVENT.blur, blurFunc, false);
      src.addEventListener(EVENT.keydown, escFunc, false);
    }

    /**
     * @example None.
     * @param {string} src - An event object.
     * @returns {undefined} Nothing.
     */
    function shake(src) {
      var li = src.parentElement;
      var id = "chartNode" + li.id.substring(li.id.indexOf("_"));
      var obj = document.getElementById(id).firstChild;
      obj.scrollIntoView();
      obj.style["-webkit-animation"] = "shake 5s";
      obj.addEventListener(
        "webkitAnimationEnd",
        function() {
          obj.style["-webkit-animation"] = CONSTANT.empty;
        },
        false
      );
    }

    /**
     * @example None.
     * @param {string} src - An event object.
     * @returns {undefined} Nothing.
     */
    function expandNode(src) {
      if (componentMap.getChart().contains(src)) {
        var dn = src.className.indexOf(CLASSNAME.contract) >= 0;
        addStyle(
          src,
          dn ? null : CLASSNAME.contract,
          dn ? CLASSNAME.contract : null
        );
        src.scrollIntoView();
      }
    }

    /**
     * @example None.
     * @returns {undefined} Nothing.
     */
    function createNavTree() {
      var navtree = componentMap.getNavTree();
      if (navtree.hasChildNodes()) {
        navtree.removeChild(navtree.firstChild);
      }
      navtree.appendChild(componentMap.getChart().children[0].cloneNode(true));
      setNavId(navtree);
    }

    /**
     * @example None.
     * @param {HTMLElement} obj - An HTML object.
     * @returns {undefined} Nothing.
     */
    function setNavId(obj) {
      if (obj.hasChildNodes()) {
        for (var ix = 0; ix < obj.childNodes.length; ix++) {
          var child = obj.childNodes[ix];
          if (child.tagName === Tag.LI) {
            var id = "navNode" + child.id.substring(child.id.indexOf("_"));
            child.setAttribute("id", id);
          }
          if (child.hasChildNodes()) {
            setNavId(child);
          }
        }
      }
    }

    /**
     * @example None.
     * @param {*} cuMap - A Copper map.
     * @returns {undefined} Nothing.
     */
    function newMap(cuMap) {
      appBody.innerHTML = "";
      appBody.appendChild(cuMap.getNavTree());
      appBody.appendChild(cuMap.getChart());
      appBody.appendChild(cuMap.getDetails());

      componentMap = cuMap;
      navtree = cuMap.getNavTree();
      createNavTree();
    }

    /**
     * @example None.
     * @returns {undefined} Nothing.
     */
    function configureUi() {
      if (optionsMenu.hasNavTree.checked) {
        if (optionsMenu.hasDetails.checked) {
          document.body.className = "everything";
        } else {
          document.body.className = "default";
        }
      } else if (optionsMenu.hasDetails.checked) {
        document.body.className = "detailsview";
      } else {
        document.body.className = "fullchart";
      }
    }

    /*
     * ====================
     * Instantiable Objects
     * ====================
     */

    /**
     * @example None.
     * @param {HTMLElement} obj - An event object.
     * @returns {undefined} Nothing.
     */
    function Dragger(obj) {
      var _this = this;
      _this.target = obj;
      _this.lastX = 0;
      _this.lastY = 0;
      _this.isEnabled = false;

      _this.onMouseDown = function(event) {
        if (!_this.isEnabled) {
          return;
        }
        if (event.target.className === CLASSNAME.node) {
          return;
        }
        _this.lastX = event.clientX;
        _this.lastY = event.clientY;
        _this.target.onmousemove = _this.onMouseMove;
      };

      _this.onMouseMove = function(event) {
        if (event.target.className === CLASSNAME.node) {
          return;
        }
        if (!_this.isEnabled) {
          return;
        }
        _this.target.scrollLeft -= event.clientX - _this.lastX;
        _this.target.scrollTop -= event.clientY - _this.lastY;
        _this.lastX = event.clientX;
        _this.lastY = event.clientY;
      };

      _this.onMouseUp = function() {
        _this.target.onmousemove = null;
      };

      _this.enable = function(enable) {
        _this.isEnabled = enable;
        if (enable) {
          addStyle(_this.target, CLASSNAME.drag);
        } else {
          addStyle(_this.target, null, CLASSNAME.drag);
        }
      };

      _this.target.onmousedown = _this.onMouseDown;
      _this.target.onmouseup = _this.onMouseUp;
    }

    /**
     * @example None.
     * @returns {*} An instance of Cu36Menu.
     */
    function OptionsMenu() {
      var mnu = new Cu63Menu();
      mnu.hasShadows = mnu.addCheckItem("Use Shadows", true, function(event) {
        mnu.hide();
        showShadows(event.target.checked);
      });
      mnu.hasBoxes = mnu.addCheckItem("Use Boxes", true, function(event) {
        mnu.hide();
        showBoxes(event.target.checked);
      });
      mnu.hasDrag = mnu.addCheckItem("Enable drag", true, function(event) {
        mnu.hide();
        enableDrag(event.target.checked);
      });
      mnu.addDividor();
      mnu.hasNavTree = mnu.addCheckItem("Navigation Tree", true, function() {
        mnu.hide();
        configureUi();
      });
      mnu.hasDetails = mnu.addCheckItem("View Details", false, function() {
        mnu.hide();
        configureUi();
      });
      return mnu;
    }

    /**
     * @example None.
     * @param {string} classname - The class name to be used.
     * @returns {undefined} Nothing.
     */
    function Cu63Menu(classname) {
      var _this = this;
      var target;
      var mnu = document.createElement(Tag.DIV);
      mnu.className = classname ? classname : CLASSNAME.contextMenu;

      this.addItem = function(text, func) {
        mnu.appendChild(new MenuItem(text, func).getElement());
        return this;
      };

      this.addCheckItem = function(text, isChecked, func) {
        var ipt = document.createElement(Tag.INPUT);
        ipt.type = "checkbox";
        ipt.checked = isChecked;
        ipt.addEventListener(EVENT.click, func, false);

        var lbl = document.createElement(Tag.LABEL);
        lbl.className = CLASSNAME.menuItem;
        lbl.appendChild(ipt);
        lbl.appendChild(document.createTextNode(text));

        mnu.appendChild(lbl);
        return ipt;
      };

      this.addDividor = function() {
        let div = document.createElement(Tag.DIV);
        div.className = CLASSNAME.dividor;
        mnu.appendChild(div);
        return this;
      };

      this.addEventListener = function(evt, func, bool) {
        mnu.addEventListener(evt, func, bool);
      };

      /**
       * @example None
       * @param {Event} evt - An event object.
       * @returns {undefined} Nothing.
       */
      var escFunc = function(evt) {
        if (evt.keyCode === KEYCODE.ESC) {
          _this.hide();
        }
      };

      this.show = function(top, left, src) {
        _this.hide();
        target = src;
        /*
         * Only call with KeyDown events because:
         *   Firefox codes are unpredictable onKeyPress.
         *   cancel on keyDown prevents keyPress, but canceling keyUp does not
         */
        document.body.addEventListener(EVENT.keydown, escFunc, false);
        mnu.style.top = top + UNIT.px;
        mnu.style.left = left + UNIT.px;
        document.body.appendChild(mnu);
        mnu.firstChild.focus();
      };

      this.hide = function() {
        if (mnu && mnu.parentElement) {
          mnu.parentElement.removeChild(mnu);
          document.body.removeEventListener(EVENT.keydown, escFunc, false);
        }
      };

      this.getElement = function() {
        return mnu;
      };
      this.getTarget = function() {
        return target;
      };
      this.isVisible = function() {
        return mnu.parentElement !== null;
      };
    }

    /**
     * Represents a MenuItem used in the Copper menu bar.
     *
     * @example None.
     * @param {string} text - The text for the manu item.
     * @param {Function} func - The function to be invoked by this menu item.
     * @param {string} tag - The tag name to be created.
     * @returns {undefined} Nothing.
     */
    function MenuItem(text, func, tag) {
      var dropMenu;
      var div = document.createElement(tag ? tag : Tag.BUTTON);
      div.className = CLASSNAME.menuItem;
      div.appendChild(document.createTextNode(text));

      if (!func) {
        return;
      }

      if (typeof func === "function") {
        div.addEventListener(EVENT.click, func, false);
      } else {
        dropMenu = func;

        div.addEventListener(
          EVENT.mouseover,
          function(event) {
            if (!dropMenu.isVisible()) {
              var rect = event.target.getBoundingClientRect();
              dropMenu.show(rect.top + rect.height, rect.left);
            }
          },
          false
        );

        div.addEventListener(
          EVENT.click,
          function(event) {
            if (dropMenu.isVisible()) {
              dropMenu.hide();
            } else {
              var rect = event.target.getBoundingClientRect();
              dropMenu.show(rect.top + rect.height, rect.left);
            }
          },
          false
        );

        div.addEventListener(
          EVENT.mouseout,
          function() {
            if (
              dropMenu.isVisible &&
              event.relatedTarget !== dropMenu.getElement() &&
              !dropMenu.getElement().contains(event.relatedTarget)
            ) {
              dropMenu.hide();
            }
          },
          false
        );

        dropMenu.addEventListener(
          EVENT.mouseout,
          function(event) {
            if (
              event.relatedTarget !== div &&
              !dropMenu.getElement().contains(event.relatedTarget)
            ) {
              dropMenu.hide();
            }
          },
          false
        );
      }

      this.getElement = function() {
        return div;
      };
    }

    /**
     * @example None.
     * @param {string} newMap - .
     * @returns {*} Nothing.
     */
    function ComponentMap(newMap) {
      var currentSection, chart, navtree, details, root;

      this.getChart = function() {
        return chart;
      };
      this.getNavTree = function() {
        return navtree;
      };
      this.getDetails = function() {
        return details;
      };
      this.getRootNode = function() {
        return root;
      };
      this.createNode = function(caption) {
        return new ChartNode(caption);
      };

      /**
       * @example None.
       * @param {string} src - An HTMLElement.
       * @returns {undefined} Nothing.
       */
      function getChartNode(src) {
        if (src === null || src === componentMap.getChart()) {
          return null;
        }
        if (
          src.className === CLASSNAME.node ||
          src.className.startsWith(CLASSNAME.node + " ")
        ) {
          return src;
        }
        return getChartNode(src.parentElement);
      }

      /**
       * @example None.
       * @param {Event} event - An event object.
       * @returns {undefined} Nothing.
       */
      function onContext(event) {
        if (event.ctrlKey || event.altKey || event.shiftKey || event.metaKey) {
          return;
        }

        var src = getChartNode(event.target);
        if (src) {
          event.preventDefault();
          contextMenu.show(event.clientY - 4, event.clientX - 4, event.target);
          return false;
        }
      }

      /**
       * @example None.
       * @param {Event} event - An event object.
       * @param {boolean} show - True if the.
       * @returns {undefined} Nothing.
       */
      function showSection(event, show) {
        var src = event.target;
        if (
          src.nodeName === Tag.DIV &&
          src.className.indexOf(CLASSNAME.node) >= 0
        ) {
          if (currentSection) {
            currentSection.style.display = STYLE.none;
          }
          var txt = src.innerText
            ? src.firstChild.innerText
            : src.firstChild.textContent;
          var section = document.getElementById(txt);
          if (!section) {
            return;
          }
          section.style.display = show ? STYLE.block : STYLE.none;
          section.contentEditable = show ? CONSTANT.strTrue : CONSTANT.strFalse;
          currentSection = section;
        }
      }

      chart = document.createElement(Tag.DIV);
      chart.className = "chart threed boxes zoom70";
      chart.setAttribute("id", "chart");

      chartDragger = new Dragger(chart);
      chartDragger.enable(true);

      navtree = document.createElement(Tag.DIV);
      navtree.className = CLASSNAME.navtree;

      details = document.createElement(Tag.SECTION);
      details.className = CLASSNAME.details;
      details.setAttribute("id", "details");

      if (newMap) {
        var newChart = newMap.getElementById("chart");
        if (!newChart) {
          return null;
        }
        chart.innerHTML = newChart.innerHTML;
        root = chart.firstChild.firstChild;

        var newDetails = newMap.getElementById("details");
        if (newDetails) {
          details.innerHTML = newDetails.innerHTML;
        }
      } else {
        var ul = document.createElement(Tag.UL);
        chart.appendChild(ul);
        root = this.createNode("Root");
        ul.appendChild(root.getElement());
      }

      chart.addEventListener(
        "mouseover",
        function(evt) {
          showSection(evt, true);
        },
        false
      );
      //chart.addEventListener("mouseout", function(evt) {showSection(evt, false);}, false);

      chart.addEventListener(
        EVENT.click,
        function(event) {
          var src = event.target;

          // Hide context menu if it was not clicked.
          if (src != contextMenu) {
            contextMenu.hide();
          }

          if (src.nodeName === Tag.DIV) {
            if (src.className.indexOf(CLASSNAME.caption) >= 0) {
              // A chart node has been clicked
              if (src.contentEditable != CONSTANT.strTrue) {
                editNode(src);
              }
            } else if (src.className.indexOf(CLASSNAME.node) >= 0) {
              showSection(event, true);
            }
          }
        },
        false
      );

      chart.addEventListener(
        EVENT.contextmenu,
        function(event) {
          onContext(event);
        },
        false
      );

      navtree.addEventListener(
        EVENT.click,
        function(event) {
          var src = event.target;

          // Hide context menu if it was not clicked.
          if (src != contextMenu) {
            contextMenu.hide();
          }

          if (src.className.indexOf(CLASSNAME.caption) >= 0) {
            src = src.parentElement;
          }

          if (src.className.indexOf(CLASSNAME.node) >= 0) {
            if (navtree.contains(src)) {
              // A navtree node has been clicked
              shake(src);
            }
          }
        },
        false
      );

      navtree.addEventListener(
        EVENT.contextmenu,
        function(event) {
          onContext(event);
        },
        false
      );
    }

    /**
     * @example None.
     * @param {string} title - The title for the node.
     * @returns {undefined} Nothing.
     */
    function ChartNode(title) {
      var newLi = document.createElement(Tag.LI);
      newLi.id = CONSTANT.chartNode + nodeId++;

      var newDiv = document.createElement(Tag.DIV);
      newDiv.className = CLASSNAME.node;
      newDiv.setAttribute(ATTRIBUTE.tabindex, "0");

      var newCaption = document.createElement(Tag.DIV);
      newCaption.className = CLASSNAME.caption;
      if (newCaption.innerText) {
        newCaption.innerText = title ? title : CONSTANT.newNode;
      } else {
        newCaption.textContent = title ? title : CONSTANT.newNode;
      }

      newDiv.appendChild(newCaption);
      newLi.appendChild(newDiv);

      this.editCaption = function() {
        editNode(newDiv);
      };
      this.getElement = function() {
        return newLi;
      };
    }

    /*
     * ====================
     * Initialize listeners
     * ====================
     */
    window.addEventListener(EVENT.load, onLoad, false);
    window.addEventListener(EVENT.paste, onPaste, false);
  }

  window.Cu63 = new Cu63(window);
})(window);
